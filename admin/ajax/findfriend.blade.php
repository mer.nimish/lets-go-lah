@extends('layouts.app')

@section('content')
<div class="container">
      <div class="row breadcrumb-style">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">My Timeline</a></li>
                <li class="breadcrumb-item active" aria-current="page">Find a Friends</li>
              </ol>
            </nav>
        </div>
      </div>
      <div class="row page-titile">
        <div class="col-md-12">
          <h2>Find a Friends</h2>
        </div>
      </div>
    </div>
    <div class="container connections-container">
      <div class="row">
        <div class="col-md-12">
           <div class="container">
            <form action="{{ URL::to('/connection/findconnection')}}" name="findconnection" class="search-form">
              <div class="input-group">
                <input type="text" class="form-control" name="search_people" id="search_people" placeholder="Type to search…">
                <div class="input-group-append">
                  <button class="btn btn-success" name="searchbtn" type="submit">Search</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="connections-list">
        @if(isset($allData['allUsers']) && count($allData['allUsers']) > 0)
            <div class="row row-eq-height">
            <?php $cnt = 0; ?>
            @foreach($allData['allUsers'] as $Users)
                @if($cnt % 3 == 0)
                    </div>
                    <div class="row row-eq-height">
                @endif
                <div class="col-md-4 col-sm-6 col-sm-12 card-connection">
                  <div class="card">
                    <?php
                        if(isset($Users->profile_pic) && $Users->profile_pic != ''){
                            echo '<img class="card-img-top" style="width:107px; height:110px; border-radius: 50%;" src="'.asset('images/users/thumb/').'/'.$Users->profile_pic.'">';
                        }else{
                            echo '<img class="card-img-top" style="width:107px; height:110px;" src="'.asset('img/man-user.png').'">';
                        }
                    ?>
                    <div class="card-body text-center">
                      <a class="person-name" href="#">{{$Users->first_name}} {{$Users->last_name}}</a>
                      @if(isset($Users->state) && $User->state != '')
                      <p class="city-name">{{$Users->city}}, {{$Users->state}}</p>
                      @else
                      <p class="city-name">{{$Users->city}}</p>
                      @endif
                      <div class="biography">
                         <p>{{ str_limit($Users->description, 70) }}</p>
                      </div>
                      <div class="btn-group-tag">
                         @if(!empty($allData['userInterest'][$Users->echo_user_id]) && is_array($allData['userInterest'][$Users->echo_user_id]))
                            @foreach($allData['userInterest'][$Users->echo_user_id] as $interest)
                                <a href="javascript:void(0);" class="btn btn-white-tag">{{$interest}}</a>
                            @endforeach
                        @endif   
                      </div>
                      <p class="connection-count">
                        <a href="{{ route('userconnection', $Users->echo_user_id) }}"><strong>{{$Users->TotalConnections}}</strong>Connections</a>
                      </p>
                      <div>
                        <button type="button" echo_user_id = "{{$Users->echo_user_id}}" id="getConnected_{{$Users->echo_user_id}}" class="btn btn-primary getConnected">Connect</button> 
                      </div>
                      <a href="{{route('userprofile', $Users->echo_user_id) }}" class="view-profile-link">View Profile</a>
                    </div>
                  </div> 
                  </div> 
                <?php $cnt++; ?>

            @endforeach

            </div>
        @elseif(isset($allData['allSiteUsers']) && count($allData['allSiteUsers']) > 0)
             <div class="row row-eq-height">
            <?php $cnt = 0; ?>
            @foreach($allData['allSiteUsers'] as $User)
                @if($cnt % 3 == 0)
                    </div>
                    <div class="row row-eq-height">
                @endif
                <div class="col-md-4 col-sm-6 col-sm-12 card-connection">
                  <div class="card">
                    <?php
                        if(isset($User->profile_pic) && $User->profile_pic != ''){
                            echo '<img class="card-img-top" style="width:107px; height:110px; border-radius: 50%;" src="'.asset('images/users/thumb/').'/'.$User->profile_pic.'">';
                        }else{
                            echo '<img class="card-img-top" style="width:107px; height:110px;" src="'.asset('img/man-user.png').'">';
                        }
                    ?>
                    <div class="card-body text-center">
                      <a class="person-name" href="#">{{$User->first_name}} {{$User->last_name}}</a>
                      @if(isset($User->state) && $User->state != '' && $User->state != 'Null')
                      <p class="city-name">{{$User->city}}, {{$User->state}}</p>
                      @else
                      <p class="city-name">{{$User->city}}</p>
                      @endif
                      <div class="biography">
                        <p>{{ str_limit($User->description,70) }}</p>


                      </div>
                      <div class="btn-group-tag">
                         @if(!empty($allData['userInterest'][$User->echo_user_id]) && is_array($allData['userInterest'][$User->echo_user_id]))
                            @foreach($allData['userInterest'][$User->echo_user_id] as $interest)
                                <a href="javascript:void(0);" class="btn btn-white-tag">{{$interest}}</a>
                            @endforeach
                        @endif   
                      </div>
                      <p class="connection-count">
                        <a href="{{ route('userconnection', $User->echo_user_id) }}"><strong>{{$User->TotalConnections}}</strong>Connections</a>
                      </p>
                      <div>
                        <button type="button" echo_user_id = "{{$User->echo_user_id}}" id="getConnected_{{$User->echo_user_id}}" class="btn btn-primary getConnected">Connect</button> 
                      </div>
                      <a href="{{route('userprofile', $User->echo_user_id) }}" class="view-profile-link">View Profile</a>
                    </div>
                  </div>
                </div>
                <?php $cnt++; ?>
            @endforeach
          </div>
        @endif
        
      </div>
      <div class="row pagination-bottom">
        
          <div class="col-lg-9">
            <nav id="pagination">
              @if(isset($allData['allUsers']) && $allData['allUsers'] != '')
                {{ $allData['allUsers']->links('connection.pagination') }}
              @else
                {{ $allData['allSiteUsers']->links('connection.pagination') }}
              @endif
            </nav>
          </div>
          
        </div>
    </div>
@endsection
