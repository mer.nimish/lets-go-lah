// Edit page location
 var lockedDrag=lockedMove=false;
    var timeoutDrag=timeoutMove=false;

    $(".form_property_localisation").click(function () {
        if (!map) {
          $("#property_approxBool").checkboxradio();
            window.setTimeout(function () {
                setupMap();
            }, 500);
        }
    });

    $(window).load(function () {

        if ($("#form_property_localisation.active").length) {
            window.setTimeout(function () {
                setupMap();
            }, 500);
        }
    });
    
    if ($('#property_country').val()>0 && $('#property_country').val() != 141) {
        alert("tab loaded"); 
    $(".group-building").addClass("hidden");
    $(".group-district").addClass("hidden");
    }
    else{
        setupBuilding();
    }
    // if ($('#property_country').val()==141) {
    // $(".group-administrative").addClass("hidden");
    // $(".group-district").removeClass("hidden");
    // }
    
    $("#property_country").change(function () {
        $("#address_streetview").html("");
        //$("#property_city").attr("data-country", $(this).val());
        if ($(this).val() != $("#property_city").attr("data-country")) {
            //alert('hi');
            $("#property_address, #property_city_other, #property_codepostal_other, #property_administrative1, #property_administrative2").val("");
            $("#property_city").val("");
            $("#property_district").val('');
            //alert($("property_district").val());
        }
        
        $("#manual").prop("checked", false).checkboxradio().change();

        if ($(this).val()==141) {
           //alert('hi');

            $(".group-district").removeClass("hidden");
            $(".group-administrative").addClass("hidden");
            $("#property_address").attr("readonly","readonly");
            $(".group-address .input-group-addon").addClass("hidden");
            $(".group-building").removeClass("hidden");
            $("#district_other").val("").closest(".form-group").addClass("hidden");
            $("#property_city_other").val("monaco");
            $("#property_codepostal_other").val("98000");    
            setupBuilding();
        }
        else if ($(this).val()>0) {
            $("#property_district").prop("disabled", false).removeClass("readonly").selectpicker("refresh");
            setupAddress();
            $(".group-administrative").removeClass("hidden");
            $(".group-address").removeClass("hidden");
            $(".group-building").addClass("hidden");
            $(".group-district").addClass("hidden");            
            $(".group-details").removeClass("hidden");
            $("#property_address").prop("readonly", false);
            $(".group-address .input-group-addon").removeClass("hidden");
        }
        else {
            $(".group-address").addClass("hidden");
            $(".group-building").addClass("hidden");
            $(".group-details").addClass("hidden");
        }
    });
     $("#property_building").change(function (e) {
        $("#adresse_streetview").html("");
        //debugloc&&console.log("Immeuble change");
        if (!$(this).val() && $(this).find("option.new:selected").length) {
            //debugloc&&console.log("  nouvel immeuble sélectionné");
            // Nouvel immeuble sélectionné
            $(".group-address .input-group-addon").addClass("hidden");
            $(".group-administrative").addClass("hidden");
            //$(".switcher-manual").parent().removeClass("hidden");
            if ($("#form_property_localisation.active").length) setupAddress();
            $("#property_building_other").val("").closest(".form-group").removeClass("hidden");
            $("#property_district").prop("disabled", false).removeClass("readonly").selectpicker("refresh");
            $("#property_address").val("").prop("readonly", false).removeClass("readonly");
            //$(".infosimmeuble,.produitsimmeuble").parent().hide();
            setupDistricts();
        }
        else if ($(this).val()>0) {
            debugloc&&console.log("  immeuble partagé sélectionné");
            $(".group-address .input-group-addon").addClass("hidden");
            
            if (autocomplete) {
                google.maps.event.removeListener(autocompleteLsr);
                google.maps.event.clearInstanceListeners(autocomplete);
                $(".pac-container").remove();
                autocomplete = false;
            }

            $("#property_building_other").closest(".form-group").addClass("hidden");
            $(".group-administrative").addClass("hidden");
            var $option = $(this).find("option:selected");
            
            $("#property_building_other").val("");
            $("#property_lat").val($option.attr("data-lat"));
            $("#property_lng").val($option.attr("data-lng"));
            $("#property_address").val($option.attr("data-address")).attr("readonly", "readonly").addClass("readonly");
            //alert($option.attr("data-district"));
            $("#property_district").attr("readonly", "readonly").selectpicker('val',$option.attr("data-district")).addClass("readonly");
            //$("#property_district").selectpicker("refresh");
            if ($("#form_property_localisation.active").length) $("#produit_approxBool").prop("checked", false).change();
            //$(".infosimmeuble,.produitsimmeuble").parent().show();
        }
        else {
            debugloc&&console.log("  autre immeuble sélectionné");
            $(".group-address .input-group-addon").removeClass("hidden");
            $("#property_building_other").val("").closest(".form-group").addClass("hidden");
            if ($("#form_property_localisation.active").length) setupAddress();
            $("#property_address").prop("readonly", false).removeClass("readonly");
            //$(".infosimmeuble,.produitsimmeuble").parent().hide();
        }
    });

    var debugloc=true;
    function setupBuilding ()
    {
        var $select = $("#property_building");
        if ($select.data("setup")) return;
        $(".group-building").addClass("hidden");
            $.ajax({
            method: 'post',
            url: 'ajax/getbuilding.php',
            data: {id: 141}
            }) .done(function(data){

            $(".group-building").removeClass("hidden");
            var val = $("#property_building").val();
            //debugloc&&console.log("setupImmeubles : base val : "+val+ " - immeubles : "+data.length);
            $("#property_building").data("ville",$("#produit_ville").val());

            $("#property_building").find("option").remove();
            if (!data || !data.length) {
                debugloc&&console.log('no immeuble...');
//                $(".group-immeuble").hide();
                $("#produit_immeuble").append("<option value=''>Autre</option>");
            }
            else {

                var cpt= 0,curval=false;
                $.each( JSON.parse(data), function( k, v ) {
                    cpt++;
                    curval= k.building_id;
                    $("#property_building").append("<option value='"+(v.building_id>0? v.building_id:"")+"' data-district='"+(v.district)+"' data-address='"+(v.address?v.address.replace("'", "’"):"")+"' data-lat='"+(v.latitude)+"' data-lng='"+(v.longitude)+"'>"+ v.building_name+"</option>");
                });

                // if ($("#property_building option[data-prio]").length) {
                //     $('<option data-divider="true" value="-1" disabled></option>').insertBefore($("#produit_immeuble option").first());
                //     $("#produit_immeuble option[data-prio]").each(function () {
                //         $(this).insertBefore($("#property_building option[data-divider='true']").first());
                //     });
                // }
                $("#property_building").append("<option data-divider='true'></option>");
                $("#property_building").append("<option value=''>Autre</option>");
                $("#property_building").append("<option value='' class='new'>[+] Nouvel immeuble</option>");

//                $(".group-immeuble").show();
                $("#property_building").val(val?val:"").selectpicker("refresh");
                $("#property_building").change();
                $("#property_district").selectpicker("refresh");
            }
            //debugloc&&console.log("setupImmeubles : set val : "+val);
        });
    }
    $("#property_city_other, #property_city").change(function () {

        if ($("#property_city_other").val() || $("#property_city").val()) {
            $(".alert-no-city").removeClass("hidden");
        }
        else {
            $(".alert-no-city").addClass("hidden");
        }
        //setupDistricts();
    });


    function setupDistricts ()
    {
        if (!$("#property_city").val() && !$("#property_city_other").val()) {
            
            $(".group-district").addClass("hidden");
            $("#property_district option").remove();
            $("#property_district").val("").selectpicker("refresh");
            $("#property_district").change();
            $("#property_district").attr("data-city", 0);
        }
        else {          
            if ($("#property_district").attr("data-city ") != $("#property_city_other").val() && $("#property_city_other").val()) {
                //alert("hi");
                    var city_id = $('#property_city_other').val();
                    $.ajax({
                    method: 'post',
                    url: 'ajax/getdistrict.php',
                    data: {id: 45}
                    }) .done(function(data) {
                        $("#property_district").selectpicker("refresh");
                    var val = $("#property_district").val();
                
                    $("#property_district").attr("data-city",$("#property_city_other").val());


                    $("#property_district option").remove();
                    if (!data) {
                        
                        $(".group-district").addClass("hidden");
                    }
                    else {
                       
                        if ($("#form_property_localisation #property_country").val() > 0) {
                            $("#property_district").append("<option value=''>other</option>");
                        }

                        var cpt= 0,curval=false, city_code=false;
                        $.each( JSON.parse(data), function( k, v ) {
                            cpt++;
                            //if (v.city_code) city_code = v.city_code;
                            curval= v.district_id;
                            if (v.district_id && v.district_name) {
                                $("#property_district").append("<option value='"+(v.district_id>0? v.district_id:"")+"'>"+ v.district_name+"</option>");

                            }
                        });
                        if ($("#form_property_localisation #property_country").val() != 141) { // Pas Monaco
                          
                            if ($("#form_property_localisation #property_city_other").val().length > 0) {
                                if (!$("#property_district option[value='']").length) {  
                                    $("#property_district").append("<option value=''>other</option>");
                                }
                                if (!$("#property_district option.new").length && !city_code) {
                                    $("#property_district").append("<option data-divider='true'></option><option value='' class='new'>[+] Nouveau district</option>");
                                }

                            }
                        }
                        else if (!val && cpt==1 && curval) {
                            val = curval;
                        }

                        $("#property_district").selectpicker("refresh");
                        $(".group-district").removeClass("hidden");
                    }
             
                    $("#property_district").val(val?val:"").selectpicker("refresh");
                    $("#property_district").change();
                });
            }
            else {
                if (!$("#property_city").val()) {
                    
                    $("#property_district").attr("data-city", 0);
                }
            
                $(".group-district").removeClass("hidden");
                if (!$("#property_district option[value='']").length) {
                    $("#property_district").append("<option value='' selected>other</option>");
                }
                if ($("#form_property_localisation #property_country").val() == 141) {
                    alert("hi");
                    $("#property_district").selectpicker("refresh");
                }
                else {
                    if (!$("#property_city").attr("data-code")) {
                       
                        if (!$("#property_district option.new").length) {
                            $("#property_district").append("<option value='' class='new'>[+] Nouveau district</option>");
                        }
                        $("#property_district").selectpicker("refresh");
                    }
                }
                $("#property_district").change();
            }
        }
    }
    //$("#property_district_other").closest(".form-group").hide();
    // $("#property_district").change(function () {
        
    //     if ($(this).find("option:selected").hasClass("new")) {
    //         $("#property_district_other").closest(".form-group").show();
    //     }
    //     else {
    //         $("#property_district_other").closest(".form-group").hide();
    //     }
    // });

    var placeSearch, autocomplete, autocompleteLsr;
    $("#manual").change(function () {
        setupAddress();
    });
    function setupAddress()
    {
        var city_editable = $("#manual").is(":checked") && $("#property_country").val()>1;
        $("#property_city_other").prop("readonly", !city_editable);
        $("#property_codepostal_other").prop("readonly", !city_editable);
        $("#property_administrative1").closest('.form-group').toggleClass("hidden", city_editable);
        $("#property_administrative2").closest('.form-group').toggleClass("hidden", city_editable);
        if ($("#manual").is(":checked")) {
            $("#property_administrative1, #property_administrative2").val("");
            if (autocomplete) {
                google.maps.event.removeListener(autocompleteLsr);
                google.maps.event.clearInstanceListeners(autocomplete);
                $(".pac-container").remove();
                autocomplete = false;
            }
            return;
        }
        var autocompleteRestrict = {'country': $("#property_country option:selected").attr("data-code")};
        if (!autocomplete) {
            autocomplete = new google.maps.places.Autocomplete(
                document.getElementById('property_address'),
                {
                    types: ['address'],
                    componentRestrictions: autocompleteRestrict
                }
            );
           
            autocompleteLsr = autocomplete.addListener('place_changed', handleaddress);


        }
        else {
            autocomplete.componentRestrictions = autocompleteRestrict;
        }
    }
    var timeout_addressHasResults=false;
    $("#property_address").on("focus keyup", function () {
        var $manual = $(".pac-item-manual");
        if ($(this).val().length>4) {
            $(".zone-manual").removeClass("hidden");
        }
        else {
            $(".zone-manual").addClass("hidden");
        }
        if (!$manual.length) {
            var $manual = $('<div class="pac-item pac-item-manual"><span class="pac-icon pac-icon-marker"></span><span class="pac-item-query">address manuelle</a></span></div>');
            $manual.on("mousedown", function () {
                $("#manual").prop("checked", true).checkboxradio().change();
                $("#property_address").blur();
            });
        }
        window.setTimeout(function () {
            if (!$(".pac-container .pac-item-manual").length) $(".pac-container").append($manual);
            $(".pac-container").css("display", "block");
        }, 500);
    });

    function handleaddress () {
        var hasResults = true;
        $("#property_city_other").prop("readonly", hasResults);
        $("#property_codepostal_other").prop("readonly", hasResults);
        $("#property_administrative1").closest('.form-group').toggleClass("hidden", !hasResults);
        $("#property_administrative2").closest('.form-group').toggleClass("hidden", !hasResults);
        var place = autocomplete.getPlace();

        if (!place.geometry) {
            alert(place.name);
            return;
        }


        $("#property_approxBool").prop("checked", false).checkboxradio();
        $("#property_lat").val(place.geometry.location.lat);
        $("#property_lng").val(place.geometry.location.lng);
        $("#property_approxLat").val("");
        $("#property_approxLng").val("");
        setupMap();
        window.setTimeout(function () {
            getdistrictFromLocation(place.geometry.location);
        }, 750);
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        }

        $("#property_city_other").val("");$("#property_city").val("");
        $("#property_codepostal_other").val("");$("#property_codepostal").val("");
        $("#administrative_area_level_1").val("");$("#property_city").val("");
        $("#administrative_area_level_2").val("");$("#property_city").val("");
        var street_number, route = "";

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            var val = place.address_components[i]["long_name"];
            if (addressType=="locality") {
                $("#property_city_other").val(val);
                $("#property_city").val("");
                $(".group-city").removeClass("hidden");
            }
            else if (addressType=="postal_code") {
                $("#property_codepostal_other").val(val);
                $("#property_codepostal").val("");
                $(".group-codepostal").removeClass("hidden");

            }
            else if (addressType=="administrative_area_level_1") {
                $("#property_administrative1").val(val);
            }
            else if (addressType=="administrative_area_level_2") {
                $("#property_administrative2").val(val);
            }
            else if (addressType=="street_number") {
                street_number = val;
            }
            else if (addressType=="route") {
                route  = val;
            }
            else {

            }
        }
        if (route) {

        }
        $("#property_city").change();

    }
    function getdistrictFromLocation (location)
    {
        $.getJSON( "/ajax/get?target=entity_district&city_nom="+$("#property_city_other").val()+"&code_postal="+$("#property_codepostal_other").val(), function( data ) {
            $.each( data, function( k, v ) {

                if (v.geoshape) {
                    var geoshape = v.geoshape;
                    var geotest = geoshape.substring(0, 4);
                    geoshape = eval(geoshape);
                    if (geotest=="[[[[") geoshape = geoshape[0][0];
                    else geoshape = geoshape[0];

                    var path = [];
                    for (var i=0; i<geoshape.length; i++) {//
                        var coords = {};
                        coords.lat = geoshape[i][1];
                        coords.lng = geoshape[i][0];
                        path.push(coords);
                    }
                    var polygon = new google.maps.Polygon({
                        paths: path
                    });
          if (google.maps.geometry.poly.containsLocation(location, polygon)) {
                        
                        if ($("#property_district option[value="+v.id+"]").length) {
                           
                            $("#property_district").selectpicker("val", v.id);
                        }
                        else {
                          
                            $("#property_district").append("<option value="+v.id+" selected>"+v.nom+"</option>");
                            $("#property_district").selectpicker("val", v.id);
                        }


                        return false;
                    }
                }
            });
        });
    }
    var map, geocoder, sv, panorama, marker, circle, panoramaOptions;
    function setupMap ()
    {
        var lat = $("#property_lat").val();
        var lng = $("#property_lng").val();
        var heading = $("#property_heading").val();
        var pitch = $("#property_pitch").val();
        var zoom = parseInt($("#property_zoom").val())>0?parseInt($("#property_zoom").val()):15;
        var approx = $("#property_approxBool:checked").length;
        var approxLat = $("#property_approxLat").val();
        var approxLng = $("#property_approxLng").val();

        if (approx) {
            togglePano(false);
            $(".noapprox").addClass("hidden");
            $(".inapprox").removeClass("hidden");
            if (map && map.zoom>14) map.setZoom(14);
        }
        else {
            togglePano(true);
            $(".noapprox").removeClass("hidden");
            $(".inapprox").addClass("hidden")
        }

        if (!lat && approxLat && !approx) {
            lat = approxLat;
            lng = approxLng;
            $("#property_lat").val(lat);
            $("#property_lng").val(lng);
            $("#property_approxLat").val("");
            $("#property_approxLng").val("");
        }
        else if (lat && !approxLat && approx) {
            approxLat = lat;
            approxLng = lng;
            $("#property_lat").val("");
            $("#property_lng").val("");
            $("#property_approxLat").val(approxLat);
            $("#property_approxLng").val(approxLng);
        }

        var center = false;

        if ($("#property_building").val()>0 && $("#property_country").val()==1 && Math.abs(lat)>0 && Math.abs(lng)>0) {
            // Monaco
            center = {lat: parseFloat(lat), lng: parseFloat(lng)}
        }
        else if ($("#property_approxBool").is(":checked") && Math.abs(approxLat)>0 && Math.abs(approxLng)>0) {

            center = {lat: parseFloat(approxLat), lng: parseFloat(approxLng)}
        }
        else if (!$("#property_approxBool").is(":checked") && Math.abs(lat)>0 && Math.abs(lng)>0) {
            center = {lat: parseFloat(lat), lng: parseFloat(lng)}
        }
        if (!center) {

            return;
        }
        if (!map) {
            var mapStyles = [
                {
                    featureType: "poi",
                    elementType: "labels",
                    stylers: [
                        { visibility: "off" }
                    ]
                }
            ];
            var mapOptions = {
                center: center,
                zoom: zoom,
                draggable: true,
                streetViewControl: false,
                styles: mapStyles
            };
            map = new google.maps.Map(document.getElementById("map"), mapOptions);
            geocoder = new google.maps.Geocoder;


            sv = new google.maps.StreetViewService();
            panoramaOptions = {
                panControl: true,
                addressControl: false,
                linksControl : false,
                fullscreenControl: false
            };
            $("#address_streetview").click(function () {
                $("#property_address").val($(this).html()).trigger("focus");
            });
            panorama = new google.maps.StreetViewPanorama(document.getElementById("pano"), panoramaOptions);
        }
        else {
            map.setCenter(center);
        }

        if (marker) {
            marker.setPosition(center);
        }
        else {
            marker = new google.maps.Marker({
                position: center,
                draggable: true,
                icon: "/sparkandpartners/img/placeholder.png",
                map: map
            });
        }

        if (circle) {
            circle.setCenter(center);
        }
        else {
            circle = new google.maps.Circle({
                strokeColor: '#999999',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FFFFFF',
                fillOpacity: 0.5,
                center: center,
                radius: 500,
                draggable: true,
                map: map
            });
        }


        if ($("#property_building").val()>0 && $("#property_country").val()==141 && !$("#property_approxBool").is(":checked")) {
            if (marker) marker.draggable = $("#property_building").find("option[data-prio=1]:selected").length>0;
            if (circle) circle.draggable = false;
            if (marker) marker.setMap(map);
            if (circle) circle.setMap(null);
            $(".approxSwitch").toggleClass("hidden", !marker.draggable);

            panoramaOptions = {
                location: marker.getPosition(),
                source: google.maps.StreetViewSource.OUTDOOR
            };
            sv.getPanorama(panoramaOptions, processSVData);

        }
        else if ($("#property_approxBool").is(":checked")) {

            if (marker) marker.setMap(null);
            if (circle) circle.setMap(map);
            if (marker) marker.draggable = true;
            if (circle) circle.draggable = true;
            $(".approxSwitch").removeClass("hidden");
        }
        else {

            if (marker) marker.setMap(map);
            if (circle) circle.setMap(null);
            if (marker) marker.draggable = true;
            if (circle) circle.draggable = true;
            $(".approxSwitch").removeClass("hidden");

            if (0) {
                panoramaOptions = {
                    position: marker.getPosition(),
                    panControl: false,
                    addressControl: false,
                    fullScreenControl: false
                };
                if (parseFloat(heading) && parseFloat(pitch)) {
                    panoramaOptions.pov = {
                        heading: parseFloat(heading),
                        pitch: parseFloat(pitch)
                    }
                }
                panorama = new google.maps.StreetViewPanorama(document.getElementById("pano"));//, panoramaOptions);
            }
            panoramaOptions = {
                location: marker.getPosition(),
                source: google.maps.StreetViewSource.OUTDOOR
            };
            sv.getPanorama(panoramaOptions, processSVData);
        }

        google.maps.event.addListener(map, 'zoom_changed', function() {
            $("#property_zoom").val(map.getZoom());
        });



        google.maps.event.addListener(circle, 'dragend', function() {
            if (circle.center && !($("#property_building").val()>0 && $("#property_country").val()==1)) {
                approxLat = Math.round(Math.pow(10,6)*circle.center.lat())/Math.pow(10,6);
                approxLng = Math.round(Math.pow(10,6)*circle.center.lng())/Math.pow(10,6);
                $("#property_lat").val("");
                $("#property_lng").val("");
                $("#property_approxLat").val(approxLat);
                $("#property_approxLng").val(approxLng);
            }
        });
        google.maps.event.addListener(marker, 'dragend', function() {
            if (marker.getPosition() && ($("#property_building").find("option[data-prio=1]:selected").length>0 || !($("#property_building").val()>0 && $("#property_country").val()==1))) {
                lat = Math.round(Math.pow(10,6)*marker.getPosition().lat())/Math.pow(10,6);
                lng = Math.round(Math.pow(10,6)*marker.getPosition().lng())/Math.pow(10,6);
                if (!lockedMove) {
                    $("#property_lat").val(lat);
                    $("#property_lng").val(lng);
                    $("#property_approxLat").val("");
                    $("#property_approxLng").val("");
                    lockedDrag=true;

                    panoramaOptions = {
                        location: marker.getPosition(),
                        source: google.maps.StreetViewSource.OUTDOOR
                    };
                    sv.getPanorama(panoramaOptions, processSVData);

                    if (timeoutDrag) window.clearTimeout(timeoutDrag);
                    timeoutDrag = window.setTimeout("lockedDrag=false;", 2000);
                    geocoder.geocode({'location': panorama.getPosition()}, function(results, status) {
                        if (status === 'OK') {
                            if (results[1]) {
                                $("#address_streetview").html(results[0].formatted_address);
                            }
                        }
                    });
                }
            }
        });

        google.maps.event.addListener(panorama, 'position_changed', function() {
            if (!lockedDrag && ($("#property_building").find("option[data-prio=1]:selected").length>0 || !($("#property_building").val()>0 && $("#property_country").val()==1))) {
                lockedMove=true;
                if (0) {
                    // DESACTIVE : pose des pb de lat/lng quand l'address exacte n'est pas en StreetView
                    lat = Math.round(Math.pow(10,6)*panorama.getPosition().lat())/Math.pow(10,6);
                    lng = Math.round(Math.pow(10,6)*panorama.getPosition().lng())/Math.pow(10,6);
                    marker.setPosition(panorama.getPosition());
                    $("#property_lat").val(lat);
                    $("#property_lng").val(lng);
                    $("#property_approxLat").val("");
                    $("#property_approxLng").val("");
                }
                geocoder.geocode({'location': panorama.getPosition()}, function(results, status) {
                    if (status === 'OK') {
                        if (results[1]) {
                            $("#address_streetview").html(results[0].formatted_address);
                        }
                    }
                });

                if (timeoutMove) window.clearTimeout(timeoutMove);
                timeoutMove = window.setTimeout("lockedMove=false;", 2000);
            }
        });
        google.maps.event.addListener(panorama, 'pov_changed', function() {
            if (!lockedDrag && ($("#property_building").find("option[data-prio=1]:selected").length>0 || !($("#property_building").val()>0 && $("#property_country").val()==1))) {
                $("#property_heading").val(Math.round(Math.pow(10, 6) * panorama.getPov().heading) / Math.pow(10, 6));
                $("#property_pitch").val(Math.round(Math.pow(10, 6) * panorama.getPov().pitch) / Math.pow(10, 6));
            }
        });
    }
    function processSVData(data, status) {
        if (status === 'OK') {
            panorama.setPano(data.location.pano);
            var heading = $("#property_heading").val();
            var pitch = $("#property_pitch").val();

            if (parseFloat(heading) && parseFloat(pitch)) {
                panorama.setPov({
                    heading: parseFloat(heading),
                    pitch: parseFloat(pitch)
                });
            }
            togglePano(true);
        } else {
            togglePano(false);
        }
    }

    function togglePano (show)
    {
        $(".mainpano").toggleClass("hidden", !show);
        $(".mainmap").toggleClass("full", !show);
        if (typeof panorama != "undefined" && panorama) panorama.setVisible(show);
        google.maps.event.trigger(map, "resize");

    }
    $("#property_approxBool").change(function () {
        setupMap();
    });


