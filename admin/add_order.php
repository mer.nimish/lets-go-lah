<?php
if(!isset($_COOKIE['login'])){
    header('location: index.php');
}
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;

if (isset($_POST['createorderbtn'])) {

    $pkg_title       = $_POST['pkg_title'];
    $pkg_start_price = $_POST['pkg_start_price'];
    $pkg_end_price   = $_POST['pkg_end_price'];
    $pkg_day_price   = $_POST['pkg_day_price'];
    $pkg_night_price = $_POST['pkg_night_price'];   
    $content         = $_POST['content'];
    $pkg_incl        = $_POST['pkg_incl'];

    $name       =   $_FILES['pkg_img']['name'];
    $tmp_name   =   $_FILES['pkg_img']['tmp_name'];
    $size       =   $_FILES['pkg_img']['size'];
    
    $uploadPath =   $_SERVER['DOCUMENT_ROOT'].'/lets-go-lah/img/package/'.$name;
    $name_arr   =   explode('.', $name);
    $extension  =   strtolower(end($name_arr));
    if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'bmp'){
        if ($size <= 2000000) {
            move_uploaded_file($tmp_name, $uploadPath);
            $last_inserted_id = '';
            $last_inserted_id = addPackage($pkg_title, $pkg_start_price, $pkg_end_price,$pkg_day_price,$pkg_night_price,$content,$pkg_incl,$name);
        }
    }
    
    if ($last_inserted_id > 0 && $last_inserted_id != '') {
        $message = '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Yay! Your form has been sent successfully.</p> 
						<div class="clearfix"></div>
					</div>';

    } else {
        $message = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.</p>
						<div class="clearfix"></div>
					</div>';
    }
}
$allusertypeslist = getusertypes();
include('header.php');
include('left_sidebar.php');
?>

<div class="page-wrapper">
    <div class="container-fluid ">	
        <div class="col-md-12">
            <?php if (isset($successMsg)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left"><?= $successMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorEmailMsg)) { ?>
                <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-alert-circle-o pr-15 pull-left"></i><p class="pull-left"><?= $errorEmailMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorOtherMsg)) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left"><?= $errorOtherMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
        </div>

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Create Order</h5>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Order</a></li>
                    <li class="active"><span>Create Order</span></li>
                </ol>
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Order Form</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo $message; ?>
                                    <div class="form-wrap">
                                        <form data-toggle="validator" name="createpkgform" id="createpkgform" method="post" action="#" role="form" enctype="multipart/form-data">
                                            <div class="form-group">
                                            <label for="username" class="control-label mb-10">User Name</label>
                                            <?php 
                                                $getusers = getusers();
                                                $res = mysqli_query($conn, $getusers);
                                            ?>
                                                <select id="username" name="username" class="form-control">
                                                    <option value="">Select User Name</option>
                                                    <?php  while ($row = mysqli_fetch_assoc($res)) {?>
                                                        <option value="<?php echo $row['user_id'].','.$row['first_name'].' '.$row['last_name']; ?>"><?php echo $row['first_name'].' '.$row['last_name']; ?></option> 
                                                        
                                                <?php }?>        
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="email" class="control-label mb-10">User Email Address</label>
                                                <input type="email" class="form-control" name="email" id="email" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="packagedata" class="control-label mb-10">Package</label><?php 
                                                    $getpackages = getpackages();
                                                    $res = mysqli_query($conn, $getpackages);
                                                ?>
                                                <select id="packagedata" name="packagedata" class="form-control">
                                                    <option value="">Select Budget</option>
                                                    <?php  while ($row = mysqli_fetch_assoc($res)) {?>
                                                        <option value="<?php echo $row['id'].','.$row['pkg_title'] ?>"><?php echo $row['pkg_title'] ?></option>
                                                <?php }?>        
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="budget_type" class="control-label mb-10">Budget</label><?php 
                                                    $getbudgets = getbudgets();
                                                    $res = mysqli_query($conn, $getbudgets);
                                                ?>
                                                <select id="budget_type" name="budget_type" class="form-control">
                                                    <option value="">Select Budget</option>
                                                    <?php  while ($row = mysqli_fetch_assoc($res)) {?>
                                                        <option value="<?php echo $row['id'].','.$row['budget_title'] ?>"><?php echo $row['budget_title'] ?></option>
                                                <?php }?>        
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="from_date" class="control-label mb-10">FROM </label>
                                                <input type="text" class="form-control" id="from_date" name="from_date" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="to_date" class="control-label mb-10">TO </label>
                                                <input type="text" class="form-control" id="to_date" name="to_date" required>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="person" class="control-label mb-10">No. of Person</label>
                                                <select class="form-control" id="person" name="person" required>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                </select>    
                                            </div>
                                            <div class="form-group">
                                                <label for="price" class="control-label mb-10">Price </label>
                                                <input type="number" class="form-control" id="price" name="price" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="payment" class="control-label mb-10">Payment</label>
                                                <select class="form-control" id="payment" name="payment" required>
                                                    <option value="1">Full Remaining</option>
                                                    <option value="2">Half Done & Half Remaining</option>
                                                    <option value="3">Full Done</option>
                                                </select>    
                                            </div>
                                            <div class="form-group mb-0">
                                                <input type="submit" value="Submit" name="createorderbtn" id="createorderbtn" class="btn btn-success btn-anim">
                                            </div>                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style type="text/css">
            input[type=number]::-webkit-inner-spin-button, 
            input[type=number]::-webkit-outer-spin-button { 
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                margin: 0; 
            }
        </style>


        <?php include('footer.php'); ?>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#username').on('change',function(){
                    var id = $(this).val();
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: 'ajax/getuserdata.php',
                        data: {user_id:id},
                        success: function (data) {
                            if (data != '') {
                                $("#email").val(data);
                            }
                        }
                    });
                });
                 $(function() {
                        $( "#from_date" ).datepicker({
                            dateFormat: "yy-mm-dd",
                            minDate: new Date(),
                            changeMonth: true,
                            changeYear: true
                        });
                  });
                 $(function() {
                        $( "#to_date" ).datepicker({
                            dateFormat: "yy-mm-dd",
                            minDate: new Date(),
                            changeMonth: true,
                            changeYear: true
                        });
                   });
                 
                $('#price').focus(function(){
                    var packagedata = $('#packagedata').val();
                    var budget_type = $('#budget_type').val();
                    var person = $('#person').val();
                    var to_date = $('#to_date').val();
                    var from_date = $('#from_date').val();
                    if (packagedata != '' && budget_type != '' && from_date !='' && to_date !='' && person != '') {
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: 'ajax/ajax_price.php',
                            data: {
                                    'package':packagedata,
                                    'budget':budget_type,
                                    'from_date':from_date,
                                    'to_date':to_date,
                                    'person':person
                                },
                            success: function (data) {
                                if (data != '') {
                                    alert(data);
                                }
                            }
                        });
                    }
                });
            });

        </script>
        <script>
            ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
            ClassicEditor
            .create( document.querySelector( '#pkg_incl' ) )
            .catch( error => {
                console.error( error );
            } );
            
        </script>