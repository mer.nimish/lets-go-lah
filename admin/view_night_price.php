<?php
if(!isset($_COOKIE['login'])){
    header('location: index.php');
}
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;

include('header.php');
include('left_sidebar.php');
$Pkgbudgetlist = getdatalist();
?>

<div class="page-wrapper">
    <div class="container-fluid ">          
        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Nights List</h5>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Night</a></li>
                    <li class="active"><span>Night List</span></li>
                </ol>
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-sm-12">
                <div class="response_message" style="margin-top:20px;margin-bottom:5px; margin-left:0% "></div>
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Duration Setting</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="">
                                    <table id="myTable1" class="table table-hover display pb-30" >
                                        <thead>
                                            <tr>
                                                <th>Sr No.</th>
                                                <th>Package Title</th>
                                                <th>Budget Title</th>
                                                <th>No. of Nights</th>
                                                <th>Price</th>
                                                <th>Action</th>                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (!empty($Pkgbudgetlist)) {
                                                $count=1;
                                                $res = mysqli_query($conn,$Pkgbudgetlist);
                                                while ($alluserlist = mysqli_fetch_assoc($res)) {
                                                    ?>
                                                    <tr id="row_<?=$alluserlist["id"]?>">
                                                        <td style="font-size: 20px !important;font-weight: 600;"><?= $count; ?></td>
                                                        <td style="font-size: 20px !important;font-weight: 600;"><?= $alluserlist['pkg_title']?></td>
                                                        <td style="font-size: 20px !important;font-weight: 600;"><?= $alluserlist['budget_title']?></td>
                                                        <td><?= $alluserlist['no_of_nights'];?></td>
                                                        <td><?= $alluserlist['price'];?></td>
                                                        
                                                        <td class="text-nowrap">
                                                                <a href="edit_night_price.php?id=<?php echo $alluserlist['id']; ?>" class="mr-25" data-toggle="modal" data-original-title="Edit" user_id="<?php echo $alluserlist['id']; ?>" class="editclass"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> 

                                                                <a href="#myModal" data-toggle="modal" data-original-title="Remove" night_id="<?php echo $alluserlist['id']; ?>" class="removeuser"> <i class="fa fa-close text-danger"></i> </a> 
                                                                <!-- <a href="#myModal" data-toggle="modal" data-original-title="Remove" user_id="<?php echo $alluserlist['id']; ?>" pkg_img="<?php echo $alluserlist['pkg_img']; ?>" class="removeuser"> <i class="fa fa-close text-danger"></i> </a>  -->
                                                           
                                                        </td>
                                                    </tr>
                                                    <?php $count++;
                                                }
                                            }
                                            ?>
                                        </tbody>                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title" id="myModalLabel">Confirm</h5>
                    </div>
                    <div class="modal-body">
                        <h5 class="mb-15">Are you sure want to delete this user?</h5>                        
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                        <button class="btn btn-primary" data-dismiss="modal" user_id="" id="delete_yes">Yes</button>
                    </div>                    
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <?php include('footer.php'); ?>
        <script>
            $(document).ready(function () {

                $(document).delegate('.removeuser', 'click', function () {
                    // $('#delete_yes').attr('user_id', $(this).attr('user_id'));
                    // $('#delete_yes').attr({ user_id: $(this).attr('user_id'), pkg_img:$(this).attr('pkg_img') }); 
                    $('#delete_yes').attr('night_id', $(this).attr('night_id'));
                });

                $('#delete_yes').on('click', function ()
                {
                    var id = $(this).attr('night_id');
                    var data = {
                        "action": "delete_night",
                        "id": id
                    };

                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "ajax/delete_night.php", //Relative or absolute path to response.php file
                        data: data,
                        success: function (data) {
                            $(".response_message").html(
                                    data["message"]
                                    );
                            if (data["success"] === 'true') {
                                $('#row_' + id).remove();
                            }
                        }
                    });
                });
                $('.package').addClass('active');
                $('#package').addClass('collapse in');
                $('.Vpackage').css('color', '#000');
                $('.Vpackage').css('background-color', '#0fc5bb');
                $('.Apackage').css('color', '#878787');

                $('.package').mouseover(function(){
                    $('.package').addClass('active');
                    $('.dashboard,.user,.budget,.order,.gallery').removeClass('active');
                });

                $('.dashboard').mouseover(function(){
                    $('.dashboard').addClass('active');
                    $('.user,.budget,.order,.gallery').removeClass('active');
                });
                $('.user').mouseover(function(){
                    $('.user').addClass('active');
                    $('.dashboard,.budget,.order,.gallery').removeClass('active');
                }); 
                $('.budget').mouseover(function(){
                    $('.budget').addClass('active');
                    $('.dashboard,.user,.order,.gallery').removeClass('active');
                });
                $('.order').mouseover(function(){
                    $('.order').addClass('active');
                    $('.dashboard,.user,.budget,.gallery').removeClass('active');
                });
                $('.gallery').mouseover(function(){
                    $('.gallery').addClass('active');
                    $('.dashboard,.user,.budget,.order').removeClass('active');
                });
            });
        </script> 