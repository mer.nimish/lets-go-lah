<?php
if(!isset($_COOKIE['login'])){
    header('location: index.php');
}
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;

include('header.php');
include('left_sidebar.php');
$usertypeslist = getallusertypes();
?>

<div class="page-wrapper">
    <div class="container-fluid pt-25">	        
        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">View User Type List</h5>
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-sm-12">
                <div class="response_message" style="margin-top:20px;margin-bottom:5px; margin-left:0% "></div>
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">User Type Data</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="">
                                    <table id="myTable1" class="table table-hover display pb-30" >
                                        <thead>
                                            <tr>
                                                <th>User Type</th>
                                                <th>Add contacts</th>
                                                <th>Add properties</th>
                                                <th>View properties</th>
                                                <th>Action</th>                                                
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            if (!empty($usertypeslist)) {
                                                while ($utypeslist = mysqli_fetch_assoc($usertypeslist)) {
                                                    if ($utypeslist['add_contacts'] == '1') {
                                                        $add_contacts = 'On';
                                                    } else {
                                                        $add_contacts = 'Off';
                                                    }

                                                    if ($utypeslist['add_properties'] == '1') {
                                                        $add_properties = 'On';
                                                    } else {
                                                        $add_properties = 'Off';
                                                    }

                                                    if ($utypeslist['view_properties'] == '1') {
                                                        $view_properties = 'On';
                                                    } else {
                                                        $view_properties = 'Off';
                                                    }
                                                    ?>
                                                    <tr id="row_<?=$utypeslist["user_type_id"]?>">
                                                        <td><?= $utypeslist['user_type'] ?></td>
                                                        <td><?= $add_contacts ?></td>
                                                        <td><?= $add_properties ?></td>
                                                        <td><?= $view_properties ?></td>                                                        
                                                        <td class="text-nowrap">
                                                            <?php if ($utypeslist['user_type'] != 'Super Admin') { ?>
                                                                <a href="add_user_type.php?utid=<?=$utypeslist["user_type_id"]?>" class="mr-25" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> 
                                                                <a href="#myModal" data-toggle="modal" data-original-title="Remove" user_type_id='<?php echo $utypeslist['user_type_id']; ?>' class="remove_usertype"> <i class="fa fa-close text-danger"></i> </a> 
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>	
            </div>
        </div>

        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title" id="myModalLabel">Confirm</h5>
                    </div>
                    <div class="modal-body">
                        <h5 class="mb-15">Are you sure want to delete this user type?</h5> 
                        <p>These will also delete all the users data containing of this user type.</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                        <button class="btn btn-primary" data-dismiss="modal" user_type_id="" id="delete_yes">Yes</button>
                    </div>                    
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <?php include('footer.php'); ?>
        <script>
            $(document).ready(function () {

                $(document).delegate('.remove_usertype', 'click', function () {
                    $('#delete_yes').attr('user_type_id', $(this).attr('user_type_id'));
                });

                $('#delete_yes').on('click', function ()
                {
                    var user_type_id = $(this).attr('user_type_id');
                    var data = {
                        "action": "delete_usertype",
                        "user_type_id": user_type_id
                    };
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "ajax/delete_utype.php", //Relative or absolute path to response.php file
                        data: data,
                        success: function (data) {
                            $(".response_message").html(
                                    data["message"]
                                    );
                            if (data["success"] === 'true') {
                                $('#row_' + user_type_id).remove();
                            }
                        }
                    });
                });               
            });
        </script> 