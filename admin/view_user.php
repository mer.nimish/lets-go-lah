 <?php
if(!isset($_COOKIE['login'])){
    header('location: index.php');
}
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;

include('header.php');
include('left_sidebar.php');
?>

<div class="page-wrapper">
    <div class="container-fluid">	        
        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">View User List</h5>
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-sm-12">
                <div class="response_message" style="margin-top:20px;margin-bottom:5px; margin-left:0% "></div>
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Users Data</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="">
                                    <table id="myTable1" class="table table-hover display pb-30" >
                                        <thead>
                                            <tr>
                                                <th>Sr No.</th>
                                                <th>User Image</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Status</th>
                                                <th>Created Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $userslist = getuserslist();
                                                if (!empty($userslist)) {
                                                    $count = 1;
                                                    while ($alluserlist = mysqli_fetch_assoc($userslist)) {
                                            ?>
                                            <tr id="row_<?=$alluserlist["user_id"];?>">
                                                <td style="font-size: 20px !important;font-weight: 600;"><?= $count;?></td>
                                                <td><img src="../img/users/<?= $alluserlist['user_img'];?>" height="100" width="150" ></td>
                                                <td><?= $alluserlist['first_name'] . ' ' . $alluserlist['last_name'] ?></td>
                                                <td><?= $alluserlist['email'] ?></td>
                                                <td>
                                                    <?php if($alluserlist['is_active']=='1'){?>
                                                        <a class="btn btn-success status_btn_class_active" id="active" data-uid="<?php echo $alluserlist['user_id'];?>">Active</a>
                                                    <?php }else{ ?>
                                                        <a class="btn btn-danger status_btn_class_inactive" id="inactive" data-uid="<?php echo $alluserlist['user_id'];?>">In-Active</a>
                                                    <?php } ?>
                                                </td>
                                                <td><?= date("d/m/Y", strtotime($alluserlist['created_at'])); ?></td>                                                        
                                                <td class="text-nowrap">
                                                    <?php if ($alluserlist['user_type'] == 'Super Admin') { ?>
                                                        <a href="edit_user.php?id=<?php echo $alluserlist['user_id']; ?>" class="mr-25" data-toggle="modal" data-original-title="Edit" user_id="<?php echo $alluserlist['user_id']; ?>" class="edituser"><i class="fa fa-pencil text-inverse m-r-10"></i></a> 

                                                        <a href="#myModal" data-toggle="modal" data-original-title="Remove" user_id="<?php echo $alluserlist['user_id']; ?>" user_img="<?php echo $alluserlist['user_img']; ?>" class="removeuser"> <i class="fa fa-close text-danger"></i> </a> 
                                                   <?php } ?>
                                                </td>
                                            </tr>
                                            <?php $count++; } } ?>
                                        </tbody>                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>	
            </div>
        </div>
        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title" id="myModalLabel">Confirm</h5>
                    </div>
                    <div class="modal-body">
                        <h5 class="mb-15">Are you sure want to delete this user?</h5>                        
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="false">No</button>
                        <button class="btn btn-primary" data-dismiss="modal" id="delete_yes">Yes</button>
                    </div>                    
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        
        <?php include('footer.php'); ?>
        <script>
            $(document).ready(function () {
                
                $(document).delegate('.removeuser', 'click', function () {
                    $('#delete_yes').attr({ user_id: $(this).attr('user_id'), user_img:$(this).attr('user_img') }); 
                });
                $('#delete_yes').on('click', function (){
                    var user_id = $(this).attr('user_id');
                    var user_img = $(this).attr('user_img');
                    var data = {
                        "action": "delete_user",
                        "user_id": user_id,
                        "user_img":user_img
                    };
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "ajax/delete_user.php", //Relative or absolute path to response.php file
                        data: data,
                        success: function (data) {
                            $(".response_message").html(data["message"]);
                            if (data["success"] === 'true') {
                                $('#row_' + user_id).remove();
                            }
                        }
                    });
                });
                
                $('.user').addClass('active');
                $('#user_dr').addClass('collapse in');
                $('.Vuser').css('color', '#000');
                $('.Vuser').css('background-color', '#0fc5bb');
                $('.Cuser').css('color', '#878787');

                $('.user').mouseover(function(){
                    $('.dashboard,.budget,.package,.order,.gallery').removeClass('active');
                }); 
                $('.dashboard').mouseover(function(){
                    $('.dashboard').addClass('active');
                    $('.budget,.package,.order,.gallery').removeClass('active');
                });
                $('.budget').mouseover(function(){
                    $('.budget').addClass('active');
                    $('.dashboard,.package,.order,.gallery').removeClass('active');
                });
                $('.package').mouseover(function(){
                    $('.package').addClass('active');
                    $('.dashboard,.budget,.order,.gallery').removeClass('active');
                });
                $('.order').mouseover(function(){
                    $('.order').addClass('active');
                    $('.dashboard,.budget,.package,.gallery').removeClass('active');
                });
                $('.gallery').mouseover(function(){
                    $('.gallery').addClass('active');
                    $('.dashboard,.budget,.package,.order').removeClass('active');
                });
            });

            $(document).on('click', '.status_btn_class_active', function(e) {
                    e.preventDefault();  
                    var user_id = $(this).data('uid');
                    var status = 2;
                    changestatus(status, user_id);
                });
            $(document).on('click', '.status_btn_class_inactive', function(e) {
                    e.preventDefault();  
                    var user_id = $(this).data('uid');
                    var status = 1;
                    changestatus(status, user_id);
                });
            function changestatus(status, user_id){
                $.ajax({
                    type : 'POST',
                    dataType: 'json',
                    url  : 'ajax/update_status.php',
                    data : {'user_id':user_id,'user_status':status,'action': 'update_status'},
                    success: function(data){
                        if(data == "1"){
                            $('#row_'+user_id+' a#inactive').removeClass('btn btn-danger status_btn_class_inactive');
                            $('#row_'+user_id+' a#inactive').addClass('btn btn-success status_btn_class_active');
                            $('#row_'+user_id+' a#inactive').html('Active');
                            $('a#inactive.status_btn_class_active').attr('id','active');
                        
                        }else if(data == "2"){
                            $('#row_'+user_id+' a#active').removeClass('btn btn-success status_btn_class_active');
                            $('#row_'+user_id+' a#active').addClass('btn btn-danger status_btn_class_inactive');
                            $('#row_'+user_id+' a#active').html('In-Active');
                            $('a#active.status_btn_class_inactive').attr('id','inactive');
                        
                        }else{
                            alert('Something Went wrong.');
                        }
                    }
                }); 
            }
        
    </script> 