<?php
if(!isset($_COOKIE['login'])){
    header('location: index.php');
}
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;

$id = $_REQUEST['id'];
$PkgBudgetlist = getPkgBudgetlist($id);
$package = getPackage($id);
$res = mysqli_query($conn, $package);

/* Edit Package */
if (isset($_POST['editpkgbtn'])) {
    $pkg_title       = $_POST['pkg_title'];
    $pkg_start_price = $_POST['pkg_start_price'];
    $pkg_end_price   = $_POST['pkg_end_price'];
    $content         = $_POST['content'];
    $pkg_incl        = $_POST['pkg_incl'];
    $oldimg          = $_POST['oldimg']; 

    $name       =   $_FILES['pkg_img']['name'];
    $tmp_name   =   $_FILES['pkg_img']['tmp_name'];
    $size       =   $_FILES['pkg_img']['size'];
    
    $name_arr   =   explode('.', $name);
    $first = current($name_arr).'_'.uniqid();
    $extension  =   strtolower(end($name_arr));
    $imgname =  $first.'.'.$extension;
    $uploadPath =   $_SERVER['DOCUMENT_ROOT'].'/lets-go-lah/img/package/'.$imgname;
    if($name){
        if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'bmp'){
            if ($size <= 2000000) {
                /*-- Start delete file from directory -- */
                $dirpath =   $_SERVER['DOCUMENT_ROOT'].'/lets-go-lah/img/package/';
                $dir = opendir($dirpath);
                while( ($file = readdir($dir)) !== false ) {
                    if ( $file == $oldimg) {
                         unlink($dirpath.'/'.$oldimg);
                    }
                }
                /*-- End delete file from directory -- */
                /*-- upload new selected file */
                move_uploaded_file($tmp_name, $uploadPath);
            }
        }
        $last_inserted_id = '';
        $last_inserted_id = editPackage($pkg_title, $pkg_start_price, $pkg_end_price, $content, $pkg_incl, $imgname, $id);    
    }else{
        $last_inserted_id = '';
        $last_inserted_id = editPackage($pkg_title, $pkg_start_price, $pkg_end_price, $content, $pkg_incl, $oldimg, $id);    
    }
    
    if ($last_inserted_id > 0 && $last_inserted_id != '') {
        editPkgBudget($last_inserted_id,$_POST['budget_id'],$_POST['night_price']);
        $message = '<div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Yay! Your form has been sent successfully.</p> 
                        <div class="clearfix"></div>
                    </div>';
                    header('location: view_package.php');

    } else {
        $message = '<div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.</p>
                        <div class="clearfix"></div>
                    </div>';
    }
}
$allusertypeslist = getusertypes();
include('header.php');
include('left_sidebar.php');
?>

<div class="page-wrapper">
    <div class="container-fluid"> 
        <div class="col-md-12">
            <?php if (isset($successMsg)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left"><?= $successMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorEmailMsg)) { ?>
                <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-alert-circle-o pr-15 pull-left"></i><p class="pull-left"><?= $errorEmailMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorOtherMsg)) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left"><?= $errorOtherMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
        </div>

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Edit Package</h5>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Package</a></li>
                    <li class="active"><span>Edit Package</span></li>
                </ol>
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Package Form</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-8">
                                    <?php echo $message; ?>
                                    <div class="form-wrap">
                                        <form data-toggle="validator" name="createpkgform" id="createpkgform" method="post" action="#" role="form" enctype="multipart/form-data">
                                        <?php  while ($row = mysqli_fetch_assoc($res)) { ?>
                                            <input type="hidden" name="oldimg" value="<?php echo $row['pkg_img'];?>">
                                            <div class="form-group">
                                                <label for="pkg_title" class="control-label mb-10">Package Title</label>
                                                <input type="text" class="form-control" name="pkg_title" id="pkg_title" placeholder="Package Title" value="<?php echo $row['pkg_title']; ?>" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="pkg_start_price" class="control-label mb-10">Package Start Price</label>
                                                <input type="number" class="form-control" name="pkg_start_price" id="pkg_start_price" placeholder="Package Start Price" value="<?php echo $row['pkg_start_price']; ?>" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="pkg_end_price" class="control-label mb-10">Package End Price</label>
                                                <input type="number" class="form-control" name="pkg_end_price" id="pkg_end_price" placeholder="Package End Price" value="<?php echo $row['pkg_end_price']; ?>" required>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    <label class="control-label mb-10">Budget Type</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <label class="control-label mb-10">Night Price</label>
                                                </div>
                                            </div> 
                                            <?php
                                            if (!empty($PkgBudgetlist)) {
                                                while ($data = mysqli_fetch_assoc($PkgBudgetlist)) {
                                                    //echo '<pre>';print_r($data);
                                            ?>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="hidden" class="form-control" name="budget_id[]" value="<?php echo $data['budget_id']?>" placeholder="<?php echo $data['budget_title'].' Price'?>">  
                                                        <span style="margin-left: 30px">
                                                            <?php echo $data['budget_title']?>
                                                        </span>
                                                     </div>
                                                     <div class="col-md-2">
                                                        <input type="number" class="form-control" name="night_price[]" id="night_price" placeholder="<?php echo $data['budget_title'].' Price'?>" value="<?php echo $data['night_price']?>" required>
                                                     </div>
                                                     <div class="col-md-6"></div>
                                                </div>
                                             </div>   
                                            <?php } }?>
                                            <div class="form-group">
                                                <label for="pkg_img" class="control-label mb-10">Package Image</label>
                                            </div>
                                            <div class="form-group">
                                                <img src="../img/package/<?= $row['pkg_img'];?>" height="150" width="150" >
                                                <input type="file" class="form-control" name="pkg_img" id="pkg_img">
                                            </div>
                                            <div class="img-upload-wrap gallery">
                                                <img src="" id="pkg_img_tag"/>
                                            </div><br>
                                            <div class="form-group">
                                                <textarea name="content" id="editor">
                                                    &lt;p&gt; <?php echo $row['pkg_desc']; ?> &lt;/p&gt;
                                                </textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="pkg_incl" class="control-label mb-10">Package Inclusion</label>
                                                <textarea name="pkg_incl" id="pkg_incl">
                                                    &lt;p&gt;<?php echo $row['pkg_incl']; ?> &lt;/p&gt;
                                                </textarea>
                                            </div>
                                            <div class="form-group mb-0">
                                                <input type="submit" value="Submit" name="editpkgbtn" id="editpkgbtn" class="btn btn-success btn-anim">
                                            </div>  
                                            <?php } ?>                                          
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style type="text/css">
            input[type=number]::-webkit-inner-spin-button, 
            input[type=number]::-webkit-outer-spin-button { 
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                margin: 0; 
            }
        </style>

        <?php include('footer.php'); ?>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $('form[id="createuserform"]').validate({
                rules: {
                    email: {                                               
                        remote: {
                            url: "ajax/checkemail.php",
                            type: "post"
                        }
                    }
                },
                messages: {
                    email: {                                               
                        remote: "Email already registered please use other!!"
                    }
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        </script>
        <script>
            ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
            ClassicEditor
            .create( document.querySelector( '#pkg_incl' ) )
            .catch( error => {
                console.error( error );
            } );
            $(document).ready(function() {
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#pkg_img_tag').attr('src', e.target.result).height(150).width(150);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                $("#pkg_img").change(function(){
                    readURL(this);
                });

                $("input[type=number]").on("focus", function() {
                    $(this).on("keydown", function(event) {
                        if (event.keyCode === 38 || event.keyCode === 40) {
                            event.preventDefault();
                        }
                    });
                });
                $("input[type=number]").on("focus", function() {
                    $(this).on("keydown", function(event) {
                        if (event.keyCode === 38 || event.keyCode === 40) {
                            event.preventDefault();
                        }
                    });
                });
                $('.package').addClass('active');
                $('.Vpackage').css('color', '#000');
                $('.Vpackage').css('background-color', '#0fc5bb');
                $('.Apackage').css('color', '#878787');
                $('.package').mouseover(function(){
                    $('.package').addClass('active');
                    $('.dashboard,.user,.budget,.order').removeClass('active');
                });
                $('.dashboard').mouseover(function(){
                    $('.dashboard').addClass('active');
                    $('.user,.budget,.order').removeClass('active');
                });
                $('.user').mouseover(function(){
                    $('.user').addClass('active');
                    $('.dashboard,.budget,.order').removeClass('active');
                }); 
                $('.budget').mouseover(function(){
                    $('.budget').addClass('active');
                    $('.dashboard,.user,.order').removeClass('active');
                });
                $('.order').mouseover(function(){
                    $('.order').addClass('active');
                    $('.dashboard,.user,.budget').removeClass('active');
                });
            });
        </script>