<?php
if(!isset($_COOKIE['login'])){
    header('location: index.php');
}
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;

if (isset($_POST['createbudgetbtn'])) {
    $title  = $_POST['title'];
    $desc   = $_POST['desc'];
    
    $name       =   $_FILES['budget_img']['name'];
    $tmp_name   =   $_FILES['budget_img']['tmp_name'];
    $size       =   $_FILES['budget_img']['size'];
    
    $name_arr   =   explode('.', $name);
    $first = current($name_arr).'_'.uniqid();
    $extension  =   strtolower(end($name_arr));
    $imgname =  $first.'.'.$extension;

    $uploadPath =   $_SERVER['DOCUMENT_ROOT'].'/lets-go-lah/img/budget/'.$imgname;
    
    if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'bmp'){
        if ($size <= 2000000) {
            move_uploaded_file($tmp_name, $uploadPath);
            $last_inserted_id = '';
            $last_inserted_id = addBudget($title, $desc, $imgname);
            $getpkg_id = getLastPackageId();
            $pkg_id = intval($getpkg_id) + 1;
            $addBudgetPkg = addBudgetPkg($pkg_id,$last_inserted_id);
        }
    }

    if ($last_inserted_id > 0 && $last_inserted_id != '') {
        $message = '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Yay! Your form has been sent successfully.</p> 
						<div class="clearfix"></div>
					</div>';
    } else {
        $message = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.</p>
						<div class="clearfix"></div>
					</div>';
    }
}
$allusertypeslist = getusertypes();
include('header.php');
include('left_sidebar.php');
?>

<div class="page-wrapper">
    <div class="container-fluidd">	
        <div class="col-md-12">
            <?php if (isset($successMsg)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left"><?= $successMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorEmailMsg)) { ?>
                <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-alert-circle-o pr-15 pull-left"></i><p class="pull-left"><?= $errorEmailMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorOtherMsg)) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left"><?= $errorOtherMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
        </div>

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Create New Budget</h5>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Budget</a></li>
                    <li class="active"><span>Create Budget</span></li>
                </ol>
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Budget Form</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-8">
                                    <?php echo $message; ?>
                                    <div class="form-wrap">
                                        <form data-toggle="validator" name="createbudgetform" id="createbudgetform" method="post" action="#" role="form" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="title" class="control-label mb-10">Budget Title</label>
                                                <input type="text" class="form-control" name="title" id="title" placeholder="Budget title" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="budget_img" class="control-label mb-10">Budget Image</label>
                                               <input type="file" name="budget_img" id="budget_img" required>
                                               <span class="error" style="color: red !important;"> </span>
                                            </div>
                                            <div class="img-upload-wrap gallery">
                                                <img src="" id="budget_img_tag"/>
                                            </div>
                                            <div class="form-group ck-editor__editable">
                                                <label for="desc" class="control-label mb-10">Budget description</label>
                                                <textarea name="desc" id="editor" placeholder="Budget description">
                                                    &lt;p&gt; &lt;/p&gt;
                                                </textarea>
                                            </div>    
                                            <div class="form-group mb-0">
                                                <input type="submit" value="Submit" name="createbudgetbtn" id="createbudgetbtn" class="btn btn-success btn-anim">
                                            </div>                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style type="text/css">
            input[type=number]::-webkit-inner-spin-button, 
            input[type=number]::-webkit-outer-spin-button { 
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                margin: 0; 
            }
        </style>

        <?php include('footer.php'); ?>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $('form[id="createbudgetform"]').validate({
                rules: {
                    email: {                                               
                        remote: {
                            url: "ajax/checkemail.php",
                            type: "post"
                        }
                    }
                },
                messages: {
                    email: {                                               
                        remote: "Email already registered please use other!!"
                    }
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        </script>
        <script>
            ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
            $(document).ready(function() {

                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#budget_img_tag').attr('src', e.target.result).height(150).width(150);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                $("#budget_img").change(function(){
                    readURL(this);
                });

                /* --- Start File Validation --- */
                $("#budget_img").change(function () {
                    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
                    $('.error').html('')
                    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                        alert("Only formats are allowed : "+fileExtension.join(', '));
                        $('.error').html('Valid only jpeg, jpg, png, gif and bmp file.')
                        var file = document.getElementById("budget_img");
                            file.value = file.defaultValue;
                    }
                });
                /* --- End File Validation --- */

                $("input[type=number]").on("focus", function() {
                    $(this).on("keydown", function(event) {
                        if (event.keyCode === 38 || event.keyCode === 40) {
                            event.preventDefault();
                        }
                    });
                });
                //aria-expanded="true"

                $('.budget').addClass('active');
                $('#budget').addClass('collapse in');
                $('.Abudget').css('color', '#000');
                $('.Abudget').css('background-color', '#0fc5bb');
                $('.Vbudget').css('color', '#878787');
                
                $('.budget').addClass('collapsed');

                $('.budget').mouseover(function(){
                    $('.dashboard,.user,.package,.order,.gallery').removeClass('active');
                });
                $('.dashboard').mouseover(function(){
                    $('.dashboard').addClass('active');
                    $('.user,.package,.order,.gallery').removeClass('active');
                });
                $('.user').mouseover(function(){
                    $('.user').addClass('active');
                    $('.dashboard,.package,.order,.gallery').removeClass('active');
                }); 
                $('.package').mouseover(function(){
                    $('.package').addClass('active');
                    $('.dashboard,.user,.order,.gallery').removeClass('active');
                });
                $('.order').mouseover(function(){
                    $('.order').addClass('active');
                    $('.dashboard,.user,.package,.gallery').removeClass('active');
                });
                $('.gallery').mouseover(function(){
                    $('.gallery').addClass('active');
                    $('.dashboard,.user,.package,.order').removeClass('active');
                });

            });
        </script>