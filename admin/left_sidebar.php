<?php     
?>
<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
        <li class="navigation-header">
            <span>Main</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="dashboard" href="dashboard.php" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
        </li>

        <li>
            <a href="javascript:void(0);" class="user" data-toggle="collapse" data-target="#user_dr"><div class="pull-left"><i class="fa fa-user mr-20"></i><span class="right-nav-text">Users</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="user_dr" class="collapse collapse-level-1">
                <li>
                    <a href="add_user.php" class="Cuser">Create User</a>
                </li>
                <li>
                    <a href="view_user.php" class="Vuser">View User</a>
                </li>
            </ul>
        </li>
        
        <li>
            <a href="javascript:void(0);" class="budget" data-toggle="collapse" data-target="#budget"><div class="pull-left"><i class="fa fa-tasks mr-20"></i><span class="right-nav-text">Budget</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="budget" class="collapse collapse-level-1">
                <li>
                    <a href="add_budget.php" class="Abudget">Add Budget</a>
                </li>
                <li>
                    <a href="view_budget.php" class="Vbudget">View Budget</a>
                </li>
            </ul>
        </li>
        <li >
            <a href="javascript:void(0);" class="package" data-toggle="collapse" data-target="#package"><div class="pull-left"><i class="zmdi zmdi-book mr-20"></i><span class="right-nav-text">Package</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="package" class="collapse collapse-level-1">
                <li>
                    <a href="add_package.php" class="Apackage">Add Package</a>
                </li>
                <li>
                    <a href="view_package.php" class="Vpackage">View Package</a>
                </li>
            </ul>
        </li>
        <!-- <li>
            <a href="gallery.php" class="gallery" data-toggle="collapse" data-target="#gallery"><div class="pull-left"><i class="zmdi zmdi-google-pages mr-20"></i><span class="right-nav-text">Gallery</span></div><div class="clearfix"></div></a>
        </li> -->
        <li >
            <a href="javascript:void(0);" class="night_price" data-toggle="collapse" data-target="#night_price"><div class="pull-left"><i class="zmdi zmdi-google-pages mr-20"></i><span class="right-nav-text">Night Price</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="night_price" class="collapse collapse-level-1">
                <li>
                    <a href="add_night_price.php" class="Apackage">Add Night Price</a>
                </li>
                <li>
                    <a href="view_night_price.php" class="Vpackage">View Night Price</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="view_order_package.php" class="order" data-toggle="collapse" data-target="#order"><div class="pull-left"><i class="zmdi zmdi-shopping-basket mr-20"></i><span class="right-nav-text">Order</span></div><div class="clearfix"></div></a>
        </li>
    </ul>
</div>