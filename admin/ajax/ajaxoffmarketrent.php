<?php
require '../config/config.php';
require '../model/model.php';


## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = $_POST['search']['value']; // Search value

## Custom Field value
$transectionType = $_POST['transectionType'];
$proType = implode(',', $_POST['proType']);

if(in_array("1", $_POST['statePublication'])){
  $statePublication_draft = 1;
}
if(in_array("2", $_POST['statePublication'])){
  $statePublication_offmarket = 1;
}
else{
  $statePublication_offmarket = ''; 
}
if(in_array("3", $_POST['statePublication'])){
  $statePublication_public = 1;
}
if(in_array("4", $_POST['statePublication'])){
  $statePublication_archive = 1;
}

$reference = $_POST['reference'];
$country = $_POST['country'];
$nubrooms = implode(',', $_POST['nubRooms']);
$startdate = $_POST['startDate'];
$enddate =$_POST['endDate'];

if(in_array("mixeduse", $_POST['otherCriteria']))
{
  $mixeduse = 1;
}

if(in_array("furniture", $_POST['otherCriteria']))
{
  $furniture = 1;
}

if(in_array("interesting", $_POST['otherCriteria']))
{
  $interesting = 1;
}

if(in_array("parking", $_POST['otherCriteria']))
{
  $parking = true;
}


## Search 
$searchQuery = "";


$searchQuery .= "transaction_type = '5'";   

$searchQuery .= "AND is_offmarket ='1'";

if($statePublication_draft != ''){
    $searchQuery .= "OR is_draft ='".$statePublication_draft."'";   
}
if($statePublication_public != ''){
    $searchQuery .= "OR is_public ='".$statePublication_public."'";   
}
if($statePublication_archive != ''){
    $searchQuery .= "OR is_archive ='".$statePublication_archive."'";   
}
if($transectionType != ''){
    $searchQuery .= " OR (transaction_type ='".$transectionType."' ) ";
}
if($reference != ''){
    $searchQuery .= " OR (reference like '%".$reference."%' ) ";
}
if($country != ''){
    $searchQuery .= " OR (country ='".$country."' ) ";
}
if($nubrooms != ''){
    $searchQuery .= " OR (num_rooms in (".$nubrooms.")) ";
}
if($mixeduse != ''){
    $searchQuery .= " OR (mixed_use ='".$mixeduse."' ) ";
}
if($furniture != ''){
    $searchQuery .= " OR (furnished ='".$furniture."' ) ";
}
if($interesting != ''){
    $searchQuery .= " OR (interesting_view ='".$interesting."' ) ";
}
if($parking != ''){
    $searchQuery .= " OR (num_parking !='' ) ";
}
if($startdate != '' && $enddate != ''){
    $searchQuery .= " OR (release_date BETWEEN '".date("d/m/Y", strtotime($startdate))."' AND '".date("d/m/Y", strtotime($enddate))."' ) ";
}
if($proType != ''){
    $searchQuery .= " OR (property_type in (".$proType.")) ";
}



// if($searchValue != ''){
// 	$searchQuery .= " and (emp_name like '%".$searchValue."%') ";
// }

## Total number of records without filtering
$sel = mysqli_query($conn,"select count(*) as allcount from tbl_property");
$records = mysqli_fetch_assoc($sel);

$totalRecords = $records['allcount'];

## Total number of records with filtering
$sel = mysqli_query($conn,"select count(*) as allcount from tbl_property WHERE  ".$searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
$empQuery = "select * from tbl_property WHERE  ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
//echo $empQuery;exit;
$empRecords = mysqli_query($conn, $empQuery);
$data = array();

while ($row = mysqli_fetch_assoc($empRecords)) {
    $data[] = array(
            "DT_RowId" => 'row_'.$row['property_id'],
    		    "updated_at"=>date("d/m/Y", strtotime($row['updated_at'])),
    		    "reference"=>$row['reference'],
    		    "property_title"=>$row['property_title'],
            "building_id"=>buildingName($row['building_id']),
            "property_type"=>propertyType($row['property_type']),
            "transaction_type"=>transactionType($row['transaction_type']),
            "price"=>$row['price'],
            "diffusion"=>date("d/m/Y", strtotime($row['updated_at'])),
            "num_rooms"=>$row['num_rooms'],
            "monthly_rent"=> number_format($row['price']/$row['total_area'],'2').'€/m²',
            "property_description" => transactionType($row['transaction_type']).'-'.propertyType($row['property_type']).' '.$row['num_rooms'].' rooms<br/>'.$row['property_title'],	
            "created_at" => date("d/m/Y", strtotime($row['created_at'])),
            "release_date"=> date("d/m/Y", strtotime($row['release_date'])),
            "floor" => $row['floor'],
            "photo_id" => '1',
            "total_area" => $row['total_area'].'/m²',
            "retrocession"=> $row['no_retrocession'],
            "action" => '<a href="single_property.php?propertyid='.$row['property_id'].'""><button class="btn btn-primary btn-icon-anim btn-square"><i class="fa fa-desktop"></i></button></a>
                                         <a href="edit_property.php?propertyid='.$row['property_id'].'"><button class="btn btn-default btn-icon-anim btn-square"><i class="fa fa-pencil"></i></button></a>
                                         

                                        <div class="btn-group">
                                           <button type="button" class="btn btn-default btn-icon-anim btn-square dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-caret-down"></i></button>
                                           <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                              <li class="text-left"><a href="/produit/new?id=52717" class="popup-xl" data-popup="1"><i class="fa fa-fw fa-copy"></i> Dupliquer
                                                 </a>
                                              </li>
                                              <li role="separator" class="divider"></li>
                                              <li class="text-left"><a href="#" class="disabled" disabled="disabled"><i class="fa fa-fw fa-level-up fa-rotate-90"></i><i class="fa fa-fw fa-file"></i> Draft
                                                 </a>
                                              </li>
                                              <li class="text-left"><a property_id="'.$row['property_id'].'" href="#myModal" class="ajax offmarket" data-toggle="modal" data-id="property_offmarket" data-confirm="Are you sure you want to change the status of this property?"><i class="fa fa-fw fa-level-up fa-rotate-90"></i><i class="fa fa-fw fa-low-vision"></i> Off market
                                              </a>
                                              </li>
                                              <li class="text-left"><a href="#myModal" property_id="'.$row['property_id'].'" class="public ajax " data-toggle="modal" data-id="property_public" data-confirm="Are you sure you want to change the status of this property?"><i class="fa fa-fw fa-level-up fa-rotate-90"></i><i class="fa fa-fw fa-globe"></i> Public
                                                 </a>
                                              </li>
                                              <li class="text-left"><a href="#myModal" property_id="'.$row['property_id'].'" class="archive ajax" data-toggle="modal" data-id="property_archive" data-confirm="Are you sure you want to change the status of this property?"><i class="fa fa-fw fa-level-up fa-rotate-90"></i><i class="fa fa-fw fa-archive"></i> Archive
                                                 </a>
                                              </li>
                                              <li role="separator" class="divider"></li>
                                              <li class="text-left"><a href="#myModal" property_id="'.$row['property_id'].'" class="btn-delete d-red ajax" data-toggle="modal" data-id="property_delete" data-confirm="Are you sure you want to delete this property?"><i class="fa fa-fw fa-trash"></i> Delete</a></li>
                                           </ul>
                                        </div>'
    	);
}

## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data
);

echo json_encode($response);
