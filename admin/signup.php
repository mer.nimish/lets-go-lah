<?php
include('config/config.php');
include('model/model.php');

if (isset($_POST['signupbtn'])) {

    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $email = $_POST['email'];
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    //$user_type = $_POST['user_type_id'];

    $last_inserted_id = '';
    //$last_inserted_id = addUser($first_name, $last_name, $email, $password, $user_type);
    $last_inserted_id = addUser($first_name, $last_name, $email, $password);
    if ($last_inserted_id > 0 && $last_inserted_id != '') {
        $message = '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Yay! Your form has been sent successfully.</p> 
						<div class="clearfix"></div>
					</div>';
    } else {
        $message = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.</p>
						<div class="clearfix"></div>
					</div>';
    }
}
$allusertypeslist = getusertypes();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title></title>
        <meta name="description" content="Goofy is a Dashboard & Admin Site Responsive Template by hencework." />
        <meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Goofy Admin, Goofyadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
        <meta name="author" content="hencework"/>

        <!-- Favicon -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <!-- vector map CSS -->
        <link href="../vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- bootstrap-select CSS -->
        <link href="../vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>


        <!-- Custom CSS -->
        <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <!--Preloader-->
        <div class="preloader-it">
            <div class="la-anim-1"></div>
        </div>
        <!--/Preloader-->

        <div class="wrapper pa-0">
            <header class="sp-header">
                <div class="sp-logo-wrap pull-left">
                    <a href="index.html">
                            <!-- <img class="brand-img mr-10" src="../img/logo.png" alt="brand"/> -->
                        <span class="brand-text"></span>
                    </a>
                </div>
                <div class="form-group mb-0 pull-right">
                    <span class="inline-block pr-10">Already have an account?</span>
                    <a class="inline-block btn btn-primary btn-rounded btn-outline" href="index.php">Sign In</a>
                </div>
                <div class="clearfix"></div>
            </header>

            <!-- Main Content -->
            <div class="page-wrapper pa-0 ma-0 auth-page">
                <div class="container-fluid">
                    <!-- Row -->
                    <div class="table-struct full-width full-height">
                        <div class="table-cell vertical-align-middle auth-form-wrap">
                            <div class="auth-form  ml-auto mr-auto no-float">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="mb-30">
                                            <h4 class="text-center txt-dark mb-10">Sign up Form</h4>
                                            <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
                                        </div>
                                        <?php echo $message; ?>
                                        <div class="form-wrap">
                                            <form action="#"  id="signup" name="signup" method="post">
                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="exampleInputName_1">First name</label>
                                                    <input type="text" class="form-control" name="first_name" placeholder="First name" id="first_name">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="exampleInputName_1">last name</label>
                                                    <input type="text" class="form-control" name="last_name" placeholder="Last name" id="last_name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputEmail" class="control-label mb-10">Email</label>
                                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputPassword" class="control-label mb-10">Password</label>
                                                    <div class="form-group	">
                                                        <input type="password" data-minlength="6" class="form-control" id="password" placeholder="Password" name="password">
                                                    </div>
                                                    <!-- <div class="form-group col-sm-12">
                                                            <input type="password" class="form-control" name="confirmpassword" id="inputPasswordConfirm" placeholder="Confirm" required>
                                                            <div class="help-block with-errors"></div>
                                                    </div> -->
                                                </div>

                                                <!-- <div class="form-group">
                                                    <label class="control-label mb-10">Select User Type</label>
                                                    <select class="selectpicker" data-style="form-control btn-default btn-outline" name="user_type_id">
                                                        <?php
                                                        if (isset($allusertypeslist) && $allusertypeslist != 0) {
                                                            while ($usertypes = mysqli_fetch_assoc($allusertypeslist)) {
                                                                ?>
                                                                <option value="<?=$usertypes['user_type_id'];?>"><?=$usertypes['user_type'];?></option>                                                                
                                                                <?php
                                                            }
                                                        }
                                                        ?> 
                                                    </select>
                                                </div> -->	

                                                <!-- <div class="form-group">
                                                        <div class="checkbox checkbox-primary pr-10 pull-left">
                                                                <input id="checkbox_2" required="" type="checkbox">
                                                                <label for="checkbox_2"> I agree to all <span class="txt-primary">Terms</span></label>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                </div> -->
                                                <div class="form-group text-center">
                                                    <input type="submit" value="Submit" name="signupbtn" id="signupbtn" class="btn btn-primary btn-rounded">
                                                </div>
                                            </form>
                                        </div>
                                    </div>	
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Row -->	
                </div>

            </div>
            <!-- /Main Content -->

        </div>
        <style type="text/css">
            .error{
                color: red !important;
            }</style>
        <!-- /#wrapper -->

        <!-- JavaScript -->

        <!-- jQuery -->
        <script src="../vendors/bower_components/jquery/dist/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="../vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>

        <!-- Bootstrap Select JavaScript -->
        <script src="../vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

        <!-- Slimscroll JavaScript -->
        <script src="dist/js/script.js"></script>
        <script src="dist/js/jquery.slimscroll.js"></script>

        <!-- Init JavaScript -->
        <script src="dist/js/init.js"></script>
    </body>
</html>
