<?php
if(!isset($_COOKIE['login'])){
    header('location: index.php');
}
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;
$id = $_REQUEST['id'];

$select_night = getnightdata($id);

while ($row = mysqli_fetch_assoc($select_night) ) {

    $budget_id = $row['budget_id'];
    $pkg_id = $row['pkg_id'];
    $night = $row['no_of_nights'];
    $hid = $row['id'];
}

$budget_list = getbudgets();
$pkg_list = getpackages();


if (isset($_POST['update_night_price_btn'])) {
    $price = $_POST['price'];
    $id = $_POST['hid'];

    $last_inserted_id = '';
    $last_inserted_id = editNightPrice($price, $id);
      
    if ($last_inserted_id > 0 && $last_inserted_id != '') {
        $message = '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Your Form data added successfully.</p> 
						<div class="clearfix"></div>
					</div>';
                    header('location: view_night_price.php');
    } else {
        $message = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.</p>
						<div class="clearfix"></div>
					</div>';
    }
}
$allusertypeslist = getusertypes();
include('header.php');
include('left_sidebar.php');
?>

<div class="page-wrapper">
    <div class="container-fluidd">	
        <div class="col-md-12">
            <?php if (isset($successMsg)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left"><?= $successMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorEmailMsg)) { ?>
                <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-alert-circle-o pr-15 pull-left"></i><p class="pull-left"><?= $errorEmailMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorOtherMsg)) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left"><?= $errorOtherMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
        </div>

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Add New Price</h5>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Price</a></li>
                    <li class="active"><span>Add Price</span></li>
                </ol>
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Price Form</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo $message; ?>
                                    <div class="form-wrap">
                                        <form data-toggle="validator" name="createbudgetform" id="createbudgetform" method="post" action="#" role="form" enctype="multipart/form-data">
                                            <input type="hidden" name="hid" value="<?= $hid;?>">
                                            <div class="form-group">
                                                <label for="package" class="control-label mb-10">Package Title</label>
                                                <select class="form-control" name="package" id="package" disabled="disabled">
                                                <?php 
                                                    if($pkg_list){
                                                    $res = mysqli_query($conn,$pkg_list);?>
                                                    <option value="">Select Package </option>
                                                    <?php while($row = mysqli_fetch_assoc($res)){?>
                                                        <option value="<?= $row['id'];?>" <?php if($row['id'] == $pkg_id){ echo 'Selected'; }?> ><?= $row['pkg_title'];?></option>

                                                <?php } }?>
                                                </select>
                                                <span class="error" id="pkg_title_error" style="color: red !important;"> </span>
                                            </div>

                                            <div class="form-group">
                                                <label for="budget" class="control-label mb-10">Budget Title</label>
                                                <select class="form-control" name="budget" id="budget" placeholder="Budget title" disabled="disabled">
                                                <?php 
                                                    if($budget_list){
                                                    $res = mysqli_query($conn,$budget_list);?>
                                                    <option value="">Select Budget </option>
                                                    <?php 
                                                    while($row = mysqli_fetch_assoc($res)){?>
                                                        <option value="<?= $row['id'];?>" <?php if($row['id'] == $budget_id){ echo 'Selected'; }?> ><?= $row['budget_title'];?></option>

                                                <?php } }?>
                                                </select>
                                                <span class="error" id="budget_title_error" style="color: red !important;"> </span>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="night" class="control-label mb-10">No. of Night</label>
                                                        <select class="form-control" name="night" id="night" disabled="disabled">
                                                            <option value="<?= $night;?>"><?= $night;?> </option>
                                                        </select>
                                                        <span class="error" id="night_error" style="color: red !important;"> </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="price" class="control-label mb-10">Price</label>
                                                        <input type="number" class="form-control" name="price" id="price" required >
                                                        <span class="error" id="price_error" style="color: red !important;"> </span>
                                                    </div>    
                                                </div>
                                            </div>    
                                            
                                            <div class="form-group mb-0">
                                                <input type="submit" value="Submit" name="update_night_price_btn" id="update_night_price_btn" class="btn btn-success btn-anim">
                                            </div>                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style type="text/css">
            input[type=number]::-webkit-inner-spin-button, 
            input[type=number]::-webkit-outer-spin-button { 
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                margin: 0; 
            }
        </style>

        <?php include('footer.php'); ?>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $('form[id="createbudgetform"]').validate({
                rules: {
                    email: {                                               
                        remote: {
                            url: "ajax/checkemail.php",
                            type: "post"
                        }
                    }
                },
                messages: {
                    email: {                                               
                        remote: "Email already registered please use other!!"
                    }
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        </script>
        <script>
            ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
            $(document).ready(function() {

                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#budget_img_tag').attr('src', e.target.result).height(150).width(150);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                $("#budget_img").change(function(){
                    readURL(this);
                });

                /* --- Start File Validation --- */
                $("#budget_img").change(function () {
                    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
                    $('.error').html('')
                    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                        alert("Only formats are allowed : "+fileExtension.join(', '));
                        $('.error').html('Valid only jpeg, jpg, png, gif and bmp file.')
                        var file = document.getElementById("budget_img");
                            file.value = file.defaultValue;
                    }
                });
                /* --- End File Validation --- */

                $("input[type=number]").on("focus", function() {
                    $(this).on("keydown", function(event) {
                        if (event.keyCode === 38 || event.keyCode === 40) {
                            event.preventDefault();
                        }
                    });
                });
                //aria-expanded="true"

                $('.budget').addClass('active');
                $('#budget').addClass('collapse in');
                $('.Abudget').css('color', '#000');
                $('.Abudget').css('background-color', '#0fc5bb');
                $('.Vbudget').css('color', '#878787');
                
                $('.budget').addClass('collapsed');

                $('.budget').mouseover(function(){
                    $('.dashboard,.user,.package,.order,.gallery').removeClass('active');
                });
                $('.dashboard').mouseover(function(){
                    $('.dashboard').addClass('active');
                    $('.user,.package,.order,.gallery').removeClass('active');
                });
                $('.user').mouseover(function(){
                    $('.user').addClass('active');
                    $('.dashboard,.package,.order,.gallery').removeClass('active');
                }); 
                $('.package').mouseover(function(){
                    $('.package').addClass('active');
                    $('.dashboard,.user,.order,.gallery').removeClass('active');
                });
                $('.order').mouseover(function(){
                    $('.order').addClass('active');
                    $('.dashboard,.user,.package,.gallery').removeClass('active');
                });
                $('.gallery').mouseover(function(){
                    $('.gallery').addClass('active');
                    $('.dashboard,.user,.package,.order').removeClass('active');
                });

            });
        </script>