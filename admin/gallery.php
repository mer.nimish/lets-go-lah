<!DOCTYPE html>
<?php
if(!isset($_COOKIE['login'])){
    header('location: index.php');
}
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;

include('header.php');
include('left_sidebar.php');

$budgetimg = getBudgetImg();

$pkgimg = getPkgImgs();
$userimg = getUserImg();
?>

<div class="page-wrapper">
    <div class="container-fluid">	        
        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">View Gallery</h5>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Gallery</a></li>
                    <li class="active"><span>View Gallery</span></li>
                </ol>
            </div>
        </div>
        <!-- /Title -->
    <?php if (!empty($userimg)) {?>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Users Images</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div >
                                <?php
                                    while ($data = mysqli_fetch_assoc($userimg)){
                                        if($data){?>
                                            <a href="../img/users/<?= $data['user_img']; ?>" target='_blank'data-lightbox='Gallery1'>
                                            <img src="../img/users/<?= $data['user_img']; ?>" width='150' height='150' class="Gallery1" style='margin: 0px 0px 20px 20px !important;'></a> 
                                    <?php }
                                        }
                                    ?>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    <?php } ?>
     <?php if (!empty($budgetimg)) {?>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Budgets Images</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="">
                                <?php
                                    $count  = mysqli_num_rows($budgetimg);
                                    while ($data = mysqli_fetch_assoc($budgetimg)){
                                        if($data){?>
                                            <a href="../img/budget/<?= $data['budget_img']; ?>" target='_blank' data-lightbox='Gallery2' >
                                            <img src="../img/budget/<?= $data['budget_img']; ?>" width='150' class="Gallery2" height='150' style='margin: 0px 0px 20px 20px !important;'></a> 
                                  <?php }
                                    }
                                 ?>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    <?php } ?>
    <?php if (!empty($pkgimg)) {?>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Packages Images</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="">
                                <?php
                                    while ($data = mysqli_fetch_assoc($pkgimg)){
                                        if($data){?>
                                            <a href="../img/package/<?= $data['pkg_img']; ?>" target='_blank' data-lightbox='Gallery3'>
                                            <img src="../img/package/<?= $data['pkg_img']; ?>" width='150' height='150' class="Gallery3" style='margin: 0px 0px 20px 20px !important;'></a> 
                                <?php   }
                                    }
                                 ?>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    <?php } ?>

<?php include('footer.php'); ?>
<script>
    $(document).ready(function () {
        
       $('.gallery').addClass('active');
        $('.order').mouseover(function(){
            $('.order').addClass('active');
            $('.dashboard,.user,.budget,.package,').removeClass('active');
        });
        $('.dashboard').mouseover(function(){
            $('.dashboard').addClass('active');
            $('.user,.budget,.package').removeClass('active');
        });
        $('.user').mouseover(function(){
            $('.user').addClass('active');
            $('.dashboard,.budget,.package').removeClass('active');
        }); 
        $('.budget').mouseover(function(){
            $('.budget').addClass('active');
            $('.dashboard,.user,.package').removeClass('active');
        });
        $('.package').mouseover(function(){
            $('.package').addClass('active');
            $('.dashboard,.user,.budget').removeClass('active');
        });
        $('.gallery').mouseover(function(){
            $('.gallery').addClass('active');
            $('.dashboard,.user,.package,.order').removeClass('active');
        });
    });
    lightbox.option({
          'resizeDuration': 200,
          'alwaysShowNavOnTouchDevices': false,
          'positionFromTop': 175,
          'maxWidth' : 500,
          'maxHeight' : 300,
          'wrapAround' : true
    });
</script> 