<?php 
if(!isset($_COOKIE['login'])){
    header('location: index.php');
}
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;

if (isset($_SESSION['first_name']) && isset($_SESSION['last_name'])) {
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
} else {
    echo "<script>window.location='index.php'</script>";
    exit;
}

include('header.php');
include('left_sidebar.php');

?>
		<div class="page-wrapper">
            <div class="container-fluid">	
                    <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Dashboard</h5>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                        <li class="active"><span>Morris Charts</span></li>
                    </ol>
                </div>
            </div>
            <!-- /Title -->			
				<!-- Row -->
                <div class="row">

                   <div class="col-lg-6">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div style="text-align: center !important;">
                                    <h6 class="panel-title txt-dark">Bar Chart</h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div id="morris_extra_bar_chart" class="morris-chart" style="height: 387px !important;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div style="text-align: center !important;">
                                    <h6 class="panel-title txt-dark">Donut chart ( % )</h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div id="morris_donut_chart" class="morris-chart donut-chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">Area Chart</h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div id="morris_area_chart" class="morris-chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
       </div>

    <script src="../vendors/bower_components/raphael/raphael.min.js"></script>
    <script src="../vendors/bower_components/morris.js/morris.min.js"></script>
    <script src="dist/js/morris-data.js"></script>  `
	<script type="text/javascript">
     $(document).ready(function(){
        $('.dashboard').addClass('active');
        $('.dashboard').mouseover(function(){
            $('.dashboard').addClass('active');
            $('.user,.budget,.package,.order,.gallery').removeClass('active');
        });
        $('.user').mouseover(function(){
            $('.user').addClass('active');
            $('.budget,.package,.order,.gallery').removeClass('active');
        }); 
        $('.budget').mouseover(function(){
            $('.budget').addClass('active');
            $('.user,.package,.order,.gallery').removeClass('active');
        });
        $('.package').mouseover(function(){
            $('.package').addClass('active');
            $('.user,.budget,.order,.gallery').removeClass('active');
        });
        $('.order').mouseover(function(){
            $('.order').addClass('active');
            $('.user,.budget,.package,.gallery').removeClass('active');
        });
        $('.gallery').mouseover(function(){
            $('.gallery').addClass('active');
            $('.user,.budget,.package,.order').removeClass('active');
        });
     });   
    </script>	
<?php include('footer.php');?>