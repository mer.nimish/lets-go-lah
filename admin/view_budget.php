<?php
if(!isset($_COOKIE['login'])){
    header('location: index.php');
}
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;

include('header.php');
include('left_sidebar.php');
$Budgetlist = getBudgetlist();
?>

<div class="page-wrapper">
    <div class="container-fluid">	        
        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">View Budget List</h5>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Budget</a></li>
                    <li class="active"><span>Budget List</span></li>
                </ol>
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-sm-12">
                <div class="response_message" style="margin-top:10px;margin-bottom:5px; margin-left:0% "></div>
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Budget Data</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="">
                                    <table id="myTable1" class="table table-hover display pb-30" >
                                        <thead>
                                            <tr>
                                                <th>Sr No.</th>
                                                <th>Budget Title</th>
                                                <th>Budget Description</th>
                                                <th>Image</th>
                                                <!-- <th>Created Date</th>
                                                <th>Updated Date</th> -->
                                                <th>Action</th>                                                
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            if (!empty($Budgetlist)) {
                                                $count=1;
                                                while ($alluserlist = mysqli_fetch_assoc($Budgetlist)) {

                                                    ?>
                                                    <tr id="row_<?=$alluserlist["id"]?>">
                                                        <td style="font-size: 20px !important;font-weight: 600;"><?= $count; ?></td>
                                                        <td style="font-size: 20px !important;font-weight: 600;"><?= $alluserlist['budget_title']?></td>
                                                        <td><?= $alluserlist['budget_desc'] ?></td>
                                                        <td><img src="../img/budget/<?= $alluserlist['budget_img'];?>" height="100" width="150" ></td>
                                                        <!-- <td><?= date("d/m/Y", strtotime($alluserlist['created_at'])); ?></td> -->
                                                        <!-- <td><?= $alluserlist['created_at']; ?></td>
                                                        <td><?= $alluserlist['updated_at']; ?></td> -->                                                        
                                                        <td class="text-nowrap">
                                                            <?php if ($alluserlist['user_type'] != 'Super Admin') { ?>
                                                                <a href="edit_budget.php?id=<?php echo $alluserlist['id']; ?>" class="mr-25" data-toggle="modal" data-original-title="Edit" user_id="<?php echo $alluserlist['id']; ?>" class="editclass"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> 

                                                                <a href="#myModal" data-toggle="modal" data-original-title="Remove" user_id="<?php echo $alluserlist['id']; ?>" b_img="<?php echo $alluserlist['budget_img']; ?>" class="removeuser"> <i class="fa fa-close text-danger"></i> </a> 
                                                           <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <?php $count++;
                                                }
                                            }
                                            ?>
                                        </tbody>                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>	
            </div>
        </div>
        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title" id="myModalLabel">Confirm</h5>
                    </div>
                    <div class="modal-body">
                        <h5 class="mb-15">Are you sure want to delete this user?</h5>                        
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                        <button class="btn btn-primary" data-dismiss="modal" user_id="" id="delete_yes">Yes</button>
                    </div>                    
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <?php include('footer.php'); ?>
        <script>
            $(document).ready(function () {

                $(document).delegate('.removeuser', 'click', function () {
                    //$('#delete_yes').attr('user_id', $(this).attr('user_id'));
                    $('#delete_yes').attr({ user_id: $(this).attr('user_id'), b_img:$(this).attr('b_img') }); 
                });

                $('#delete_yes').on('click', function ()
                {
                    var id = $(this).attr('user_id');
                    var b_img = $(this).attr('b_img');
                    var data = {
                        "action": "delete_budget",
                        "id": id,
                        "b_img":b_img
                    };
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "ajax/delete_budget.php", //Relative or absolute path to response.php file
                        data: data,
                        success: function (data) {
                            $(".response_message").html(
                                    data["message"]
                                    );
                            if (data["success"] === 'true') {
                                $('#row_' + id).remove();
                            }
                        }
                    });
                });
               $('.budget').addClass('active');
               $('#budget').addClass('collapse in');
                $('.Vbudget').css('color', '#000');
                $('.Vbudget').css('background-color', '#0fc5bb');
                $('.Abudget').css('color', '#878787');
                
                $('.budget').mouseover(function(){
                    $('.budget').addClass('active');
                    $('.dashboard,.user,.package,.order,.gallery').removeClass('active');
                });
                $('.dashboard').mouseover(function(){
                    $('.dashboard').addClass('active');
                    $('.user,.package,.order,.gallery').removeClass('active');
                });
                $('.user').mouseover(function(){
                    $('.user').addClass('active');
                    $('.dashboard,.package,.order,.gallery').removeClass('active');
                }); 
                $('.package').mouseover(function(){
                    $('.package').addClass('active');
                    $('.dashboard,.user,.order,.gallery').removeClass('active');
                });
                $('.order').mouseover(function(){
                    $('.order').addClass('active');
                    $('.dashboard,.user,.package,.gallery').removeClass('active');
                });
                $('.gallery').mouseover(function(){
                    $('.gallery').addClass('active');
                    $('.dashboard,.user,.package,.order').removeClass('active');
                });
            });
        </script> 