<?php

// Sign Up 
function addUser($first_name, $last_name, $email, $password, $user_type,$status,$imgname) {

    global $conn;
    $last_inserted_id = '';

    $insert_query = "INSERT INTO tbl_user SET first_name='$first_name', last_name='$last_name', email='$email', password = '$password',user_img='$imgname',user_type_id='$user_type',is_active = '$status', created_at=NOW(),updated_at=NOW()";
    mysqli_query($conn, $insert_query);
    $last_inserted_id = mysqli_insert_id($conn);
    return $last_inserted_id;
}

/* select user  */
function getUser($id) {
    global $conn;
    $query = "SELECT * FROM tbl_user WHERE user_id =$id";
    return $query;
}

/*  Update user status  */
function updateUserStatus($id,$status){
    global $conn;
    $query = "UPDATE tbl_user SET is_active = '".$status."' WHERE user_id= '".$id."' ";
    $statusUpdated = mysqli_query($conn, $query);
    
    if($statusUpdated){
        return $status;
    }
    return 0;
}

/** get all orders */
function getUserOrderList(){
    global $conn;
    $query = "SELECT * FROM tbl_user_order";
    return $query;
}

/* select users  */
function getUsers() {
    global $conn;
    // $query = "SELECT * FROM tbl_user WHERE is_delete=0 ORDER BY user_id ASC";
    $query = "SELECT * FROM tbl_user ORDER BY user_id ASC";
    return $query;
}

/* select budgets  */
function getbudgets() {
    global $conn;
    $query = "SELECT * FROM tbl_budget ORDER BY id ASC";
    return $query;
}

/* select Budget  */
function getBudget($id){
    global $conn;
    $query = "SELECT * FROM tbl_budget WHERE id =$id";
    return $query;
}

/* select Package */
function getPackage($id){
    global $conn;
    $query = "SELECT * FROM tbl_package WHERE id =$id";
    return $query;
}

/* select Package */
function getpackages(){
    global $conn;
    $query = "SELECT * FROM tbl_package ORDER BY id ASC";
    return $query;
}
/* add night price */
function addNightPrice($pkg_id, $budget_id, $night, $price)
{
    global $conn;
     $insert_query = "INSERT INTO tbl_night SET pkg_id='$pkg_id', budget_id='$budget_id', no_of_nights='$night', price = '$price', created_at=NOW()";
    $res = mysqli_query($conn,$insert_query);
    if ($res) {
        return 1;
    }
    return 0;
}

/* select night details */
function getnightdata($id){
    global $conn;
    $select_query = "SELECT * FROM tbl_night WHERE id= $id";
    $res = mysqli_query($conn,$select_query);
    return $res;
}

/* edit night price */
function editNightPrice($price, $id){
    global $conn;
    $update_query = "UPDATE tbl_night SET price='$price' WHERE id=$id";
    $res = mysqli_query($conn,$update_query);
    if ($res) {
         return 1;
    }
    return 0; 
}

function getdatalist(){
    global $conn;
    $select_query = "SELECT b.budget_title,p.pkg_title,n.id,n.no_of_nights,n.price FROM tbl_budget as b JOIN tbl_night as n ON n.budget_id = b.id JOIN tbl_package as p ON p.id = n.pkg_id";
    return $select_query;
}

/* Update User */
function editUser($first_name, $last_name, $email, $password, $user_type,$status,$id,$imgname) {
    global $conn;
    $update_query = "UPDATE tbl_user SET first_name='$first_name',last_name='$last_name',email='$email',password='$password',user_img='$imgname',user_type_id='$user_type',is_active='$status',updated_at=NOW() WHERE user_id=$id";
    $update_res = mysqli_query($conn, $update_query);
    if ($update_res) {
        return 1;
    }
    return 0;
}

/*   edit budget   */ 
function editBudget($title, $desc, $name, $id){
    global $conn;
    $update_query = "UPDATE tbl_budget SET budget_title='$title', budget_desc='$desc', budget_img='$name',updated_at=NOW() WHERE id = $id";
    $update_res = mysqli_query($conn, $update_query);
    if ($update_res) {
        return 1;
    }
    return 0;
}

/*  edit package  */ 
function editPackage($pkg_title, $pkg_start_price, $pkg_end_price, $content, $pkg_incl, $name,$id){
    global $conn;
    $update_query = "UPDATE tbl_package SET pkg_title= '$pkg_title', pkg_start_price= '$pkg_start_price', pkg_end_price= '$pkg_end_price', pkg_desc='$content',pkg_incl='$pkg_incl',pkg_img='$name', updated_at=NOW() WHERE id= $id";
    $update_res = mysqli_query($conn,$update_query);
    if ($update_res) {
        return $id;
    }
    return 0;
}

/* add budget */
function addBudget($title, $desc, $name) {
    global $conn;
    $last_inserted_id = '';
    $insert_query = "INSERT INTO tbl_budget SET budget_title ='$title', budget_desc ='$desc',budget_img='$name',created_at =NOW()";
    mysqli_query($conn, $insert_query);
    $last_inserted_id = mysqli_insert_id($conn);
    return $last_inserted_id;
}

/* List of Budget */
// function getBudgetlist() {
//     global $conn;
//     $query = "SELECT * FROM tbl_budget ORDER BY id ASC";
//     $res = mysqli_query($conn, $query);
//     if (mysqli_num_rows($res) > 0) {
//         return $res;
//     }
//     return 0;
// }

/* delete night */
function removeNightData($id){
    global $conn;
    $delete_query = "DELETE FROM tbl_night WHERE id= $id";
    echo $delete_query;die;
    $res = mysqli_query($conn,$delete_query);
    if($res){
        return 1;
    }
    return 0;
}

/* Delete budget with relational table data */
function removeBudgetData($uid) {
    global $conn;
    $delete_query = "DELETE FROM tbl_budget WHERE id = $uid";
    $res = mysqli_query($conn, $delete_query);
    if ($res) {
        return 1;
    }
    return 0;
}

/* Get Budget Image name  */
function getBudgetImg(){
    global $conn;
    $query = "SELECT budget_img FROM tbl_budget";
    $res = mysqli_query($conn, $query);
    if (mysqli_num_rows($res) > 0) {
        return $res;
    }
    return 0;
}

/* Get Package Image name  */
function getPkgImgs(){
    global $conn;
    $query = "SELECT pkg_img FROM tbl_package";
    $res = mysqli_query($conn, $query);
    if($res){
        return $res;
    }
    return 0;
}

/* Get Users Image name */
function getUserImg(){
    global $conn;
    $query = "SELECT user_img FROM tbl_user";
    $res = mysqli_query($conn, $query);
    if($res){
        return $res;
    }
    return 0;

}
/*  add package  */
function addPackage($pkg_title, $pkg_start_price, $pkg_end_price, $content,$pkg_incl, $name) {
    global $conn;
    $last_inserted_id = '';

    $insert_query = "INSERT INTO tbl_package SET pkg_title='$pkg_title',pkg_start_price='$pkg_start_price',pkg_end_price='$pkg_end_price',pkg_desc='$content',pkg_incl='$pkg_incl',pkg_img='$name' ,created_at=NOW()";
    mysqli_query($conn, $insert_query);
    $last_inserted_id = mysqli_insert_id($conn);
    return $last_inserted_id;
}

/*  add package, budget, night price  */
/*function addPkgBudget($pkg_id,$budget_id,$night_price) {*/
    /*global $conn;
    $delete_query = "DELETE FROM tbl_pkg_budget WHERE night_price=''";
    $update_res = mysqli_query($conn, $delete_query);

    $select_query = "SELECT budget_id from tbl_pkg_budget WHERE night_price='' AND pkg_id = $pkg_id";
    $select_res = mysqli_query($conn, $select_query);*/

    /* 
     while ($row = mysqli_fetch_assoc($select_res) ) {
     } */

    /*foreach($budget_id as $key => $bid){*/

        /* if($row['budget_id']== $bid){
            $update_query = "UPDATE tbl_pkg_budget SET night_price= $night_price[$key],updated_at=NOW())";
            mysqli_query($conn, $update_query);    
        }else{ */
            
            /* $insert_query = "INSERT INTO tbl_pkg_budget (pkg_id,budget_id,night_price,created_at) VALUES ($pkg_id,$bid, $night_price[$key],NOW())";
            mysqli_query($conn, $insert_query);*/
        /*}
    }
}*/

/*  select last package id  */
function getLastPackageId(){
    global $conn;
    $select_query = "SELECT id FROM tbl_package ORDER BY id DESC";
    $res = mysqli_query($conn, $select_query);
    while ($row = mysqli_fetch_assoc($res) ) {
        return $row['id'];
    }
    return 0;
}

/*  Add package  budget  */
function addBudgetPkg($pkg_id,$budget_id){
    global $conn;
    $last_id = '';
    $insert_query = "INSERT INTO tbl_pkg_budget SET pkg_id='$pkg_id', budget_id='$budget_id', created_at=NOW()";
    mysqli_query($conn, $insert_query);
    $last_id = mysqli_insert_id($conn);
    return $last_id;
}

/*  Edit package, budget, night price  */
function editPkgBudget($pkg_id,$budget_id,$night_price){
    global $conn;
    foreach($budget_id as $key => $bid){
        $update_query = "UPDATE tbl_pkg_budget SET night_price='$night_price[$key]', updated_at=NOW() WHERE pkg_id='$pkg_id' AND budget_id='$bid' ";
        mysqli_query($conn, $update_query);
    }
}

/*  select  package, budget, night price  */
function getPkgBudgetlist($id){
    global $conn;
    $query = "SELECT tu.pkg_id,tu.budget_id,tu.night_price, tut.budget_title FROM tbl_pkg_budget as tu LEFT JOIN tbl_budget as tut ON tut.id = tu.budget_id WHERE tu.pkg_id = '$id' ";
    $res = mysqli_query($conn, $query);
    if (mysqli_num_rows($res) > 0) {
        return $res;
    }
    return 0;
}

/* List of Packages */
function getPackgelist() {
    global $conn;
    $query = "SELECT * FROM tbl_package";
    $res = mysqli_query($conn, $query);
    if (mysqli_num_rows($res) > 0) {
        return $res;
    }
    return 0;
}

/* get Package  */
function getpkgById($pkg_id){
    global $conn;
    $query = "SELECT pkg_day_price,pkg_night_price from tbl_package WHERE id = $pkg_id";
    $res = mysqli_query($conn, $query);
    
}

/* Delete Package also delete from related table*/
function removePackageData($pkg_id) {
    global $conn;
    $delete_query = "DELETE FROM tbl_package WHERE id = $pkg_id";
    $update_res = mysqli_query($conn, $delete_query);
    if ($update_res) {
        return 1;
    }
    return 0;
}

//Sign in
function login($email, $password) {

    global $conn;

    $user_email = strip_tags(mysqli_real_escape_string($conn, trim($email)));
    $user_password = strip_tags(mysqli_real_escape_string($conn, trim($password)));

    $login_query = "SELECT * FROM tbl_user WHERE email = '" . $user_email . "'";

    $login_res = mysqli_query($conn, $login_query);
    if (mysqli_num_rows($login_res) == 1) {
        $rows = mysqli_fetch_array($login_res);

        $password_hash = $rows['password'];
        session_start();

        if (password_verify($user_password, $password_hash)) {
            setcookie("login", "yes", time() + (3600*24));
            $_SESSION['email'] = $rows['email'];
            $_SESSION['user_id'] = $rows['user_id'];
            $_SESSION['first_name'] = $rows['first_name'];
            $_SESSION['last_name'] = $rows['last_name'];
            $_SESSION['user_type_id'] = $rows['user_type_id'];
            $_SESSION['add_contact'] = getuserpermissiondata();  
                   
            echo "<script>window.location='dashboard.php'</script>";
        } else {
            echo "<script>window.location='index.php?login=fail'</script>";
        }
        echo "<script>window.location='dashboard.php'</script>";
    } else {
        echo "<script>window.location='index.php?login=failed'</script>";
    }
}

/*  User permission */
function getuserpermissiondata() {
    global $conn;
    $query = "SELECT * from tbl_user_type WHERE user_type_id = '".$_SESSION['user_type_id']."'";
    $res = mysqli_query($conn, $query);
    $temp = array();
    if (mysqli_num_rows($res) > 0) {        
         while ($row = mysqli_fetch_assoc($res)) {
            $temp = $row['add_contacts'];
         }        
         return $temp;
    }
    return $temp;
}

/* List of user types */
function getusertypes() {
    global $conn;
    $query = "SELECT * from tbl_user_type WHERE user_type != 'Super Admin'";
    $res = mysqli_query($conn, $query);
    if (mysqli_num_rows($res) > 0) {
        return $res;
    }
    return 0;
}

/* List of user types */
function getAdmin() {
    global $conn;
    $query = "SELECT * from tbl_user";
    $res = mysqli_query($conn, $query);
    if (mysqli_num_rows($res) > 0) {
        return $res;
    }
    return 0;
}

/* List of Users */
function getuserslist() {
    global $conn;
    // $query = "SELECT tu.user_id,tu.first_name,tu.last_name,tu.email,tu.is_active,tu.user_type_id,tu.created_at,tut.user_type from tbl_user as tu LEFT JOIN tbl_user_type as tut ON tut.user_type_id = tu.user_type_id WHERE tu.is_delete = '0' AND tu.is_active = '1' ";
    $query = "SELECT tu.user_id,tu.first_name,tu.user_img,tu.last_name,tu.email,tu.is_active,tu.user_type_id,tu.created_at,tut.user_type from tbl_user as tu LEFT JOIN tbl_user_type as tut ON tut.user_type_id = tu.user_type_id ";
    $res = mysqli_query($conn, $query);
    if (mysqli_num_rows($res) > 0) {
        return $res;
    }
    return 0;
}

/* Delete User */
function removeUserData($uid) {
    global $conn;
    $delete_query = "DELETE FROM tbl_user WHERE user_id = " . $uid;
    $delete_res = mysqli_query($conn, $delete_query);

    if ($delete_res) {
        return 1;
    }
    return 0;
}

/* Add User Type */
function addUserType($user_type, $add_contacts, $add_properties, $view_properties) {
    global $conn;
    $last_inserted_id = '';
    $insert_query = "INSERT INTO tbl_user_type SET user_type='$user_type', add_contacts='$add_contacts', add_properties='$add_properties', view_properties = '$view_properties',created_at=NOW(),updated_at=NOW()";
    mysqli_query($conn, $insert_query);
    
    $last_inserted_id = mysqli_insert_id($conn);
    return $last_inserted_id;
}

/* List of User Types */
function getallusertypes() {
    global $conn;
    $query = "SELECT * from tbl_user_type";
    $res = mysqli_query($conn, $query);
    if (mysqli_num_rows($res) > 0) {
        return $res;
    }
    return 0;
}

/* Delete User Type */
function removeUsertype($deleteusertype) {
    global $conn;
    $delete_query = "DELETE FROM tbl_user_type WHERE user_type_id = " . $deleteusertype;
    return mysqli_query($conn, $delete_query);
}

/* Get User Type By ID */
function getUserTypedatabyID($utid) {
    global $conn;
    $query = "SELECT * from tbl_user_type WHERE user_type_id = " . $utid;
    $res = mysqli_query($conn, $query);
    if (mysqli_num_rows($res) > 0) {
        return $res;
    }
    return 0;
}

/* Update User Type */
function editUserType($utid, $user_type, $add_contacts, $add_properties, $view_properties) {
    global $conn;
    $update_query = "UPDATE tbl_user_type SET user_type= '$user_type',add_contacts= '$add_contacts', add_properties= '$add_properties',view_properties = '$view_properties', updated_at=NOW() WHERE user_type_id = $utid";
    $update_res = mysqli_query($conn, $update_query);
    if ($update_res) {
        return 1;
    }
    return 0;
}

/** get all Country */
function getAllCountry() {
    global $conn;
    $query = "SELECT * from tbl_country";
    $res = mysqli_query($conn, $query);
    if (mysqli_num_rows($res) > 0) {
        return $res;
    }
    return 0;
}

/** get all City */
function getAllCity() {
    global $conn;
    $query = "SELECT * from tbl_city";
    $res = mysqli_query($conn, $query);
    if (mysqli_num_rows($res) > 0) {
        return $res;
    }
    return 0;
}

/** Get city By id (Ajax)**/
function getCityById($id){
    global $conn;
    $query = "SELECT * from tbl_city WHERE country_id = $id";
    $res = mysqli_query($conn, $query);
    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        $i = 0;
        while ($row = mysqli_fetch_array($res)) {
            $temp[$i]['city_id']= $row['city_id'];
            $temp[$i]['city_name']= $row['city_name'];
            $i++;
        }
        return $temp;
    }
    return 0;
}

/** Get district By id (Ajax)**/
function getDistrictById($id){
    global $conn;
    $query = "SELECT * from tbl_district WHERE city_id = $id";
    $res = mysqli_query($conn, $query);
    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        $i = 0;
        while ($row = mysqli_fetch_array($res)) {
            $temp[$i]['district_id']= $row['district_id'];
            $temp[$i]['district_name']= $row['district_name'];
            $i++;
        }
        return $temp;
    }
    return 0;
}

/** Get user By id (Ajax)**/
function getuserById($id){
    global $conn;
    $query = "SELECT * from tbl_user WHERE user_id = $id";
    $res = mysqli_query($conn, $query);
    if (mysqli_num_rows($res) > 0) {
        while ($row = mysqli_fetch_array($res)) {
            $email = $row['email'];
        }
        return $email;
    }
    return 0;
}

/** Get building By id (Ajax)**/
function getBuildingById($id){
    global $conn;
    $query = "SELECT * from tbl_building WHERE country = $id";
    $res = mysqli_query($conn, $query);
    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        $i = 0;
        while ($row = mysqli_fetch_array($res)) {
            $temp[$i]['building_id']= $row['building_id'];
            $temp[$i]['building_name']= $row['building_name'];
            $temp[$i]['district']= $row['district'];
            $temp[$i]['address']= $row['address'];
            $temp[$i]['latitude']= $row['latitude'];
            $temp[$i]['longitude']= $row['longitude'];
            $i++;
        }
        return $temp;
    }
    return 0;
}

/* Add property */
function addProperty($request){

    global $conn;
    if($request['typeViager'] != '' && isset($request['typeViager'])){
        $sub_transaction_type =  $request['typeViager'];
    }
    else{
        $sub_transaction_type =  $request['typeoffre'];   
    }
    if($request['address'] !='' && isset($request['address'])){
        $address =mysqli_real_escape_string($conn,$request['address']);
    }
    else{
        $address =mysqli_real_escape_string($conn,$request['building_address']);   
    }
    $city =mysqli_real_escape_string($conn,$request['city']);
    $region = mysqli_real_escape_string($conn,$request['region']);
    $district = mysqli_real_escape_string($conn,$_POST['district']);
    
    $last_inserted_id = '';
    $insert_query = "INSERT INTO tbl_property SET user_id='".$request['user_id']."', country='".$request['country']."', address='".$address."', city='".$city."',postalcode='".$request['codepostal']."',region='".$region."',district='".$district."',building_id='".$request['building']."',latitude='".$request['latitude']."',longitude = '".$request['longitude']."',transaction_type='".$request['typetransaction']."',sub_transaction_type='".$request['typeoffre']."',property_type='".$request['typeproperty']."',sub_property_type='".$request['subtypeproperty']."',num_rooms ='".$request['numrooms']."',num_bedrooms='".$request['numbedrooms']."',num_bathrooms='".$request['numsbathrooms']."',num_parking='".$request['numparking']."',num_box='".$request['numbox']."',num_cellars='".$request['numcaves']."',reference='".$request['reference']."', created_at='".date("Y-m-d H:i:s")."' ";
    //print_r($insert_query);exit;
    mysqli_query($conn, $insert_query);
    $last_inserted_id = mysqli_insert_id($conn);
    //print_r($last_inserted_id);exit;
    if($last_inserted_id > 0){
        return $last_inserted_id;
    }else{
        return 0;
    }
}

/* Get a property by id */ 
function getPropertyByID($property_id){
    global $conn;
    $query = "SELECT * from tbl_property WHERE property_id = $property_id";
    $res = mysqli_query($conn, $query);
    if (mysqli_num_rows($res) > 0) {
        while ($row = mysqli_fetch_assoc($res)) {
            $temp['reference']= $row['reference'];
            $temp['transaction_type'] = $row['transaction_type'];
            $temp['sub_transaction_type'] = $row['sub_transaction_type'];
            $temp['property_type'] = $row['property_type'];
            $temp['sub_property_type']=$row['sub_property_type'];
            $temp['location_rental']=$row['location_rental'];
            $temp['price']=$row['price'];
            $temp['monthly_rent']=$row['monthly_rent'];
            $temp['no_retrocession']=$row['no_retrocession'];
            $temp['annual_costs']=$row['annual_costs'];
            $temp['monthly_service_charge']=$row['monthly_service_charge'];
            $temp['release_date']=$row['release_date'];
            $temp['sole_agent'] = $row['sole_agent'];
            $temp['collaboration_with'] = $row['collaboration_with'];
            $temp['mixed_use']=$row['mixed_use'];
            $temp['furnished']=$row['furnished'];
            $temp['num_rooms']=$row['num_rooms'];
            $temp['num_bedrooms']=$row['num_bedrooms'];
            $temp['num_bathrooms']=$row['num_bathrooms'];
            $temp['num_parking']=$row['num_parking'];
            $temp['num_box']=$row['num_box'];
            $temp['num_cellars']=$row['num_cellars'];
            $temp['floor']=$row['floor'];
            $temp['interesting_view']=$row['interesting_view'];
            $temp['state']=$row['state'];
            $temp['living_area']=$row['living_area'];
            $temp['terrace_area']=$row['terrace_area'];
            $temp['land_area']=$row['land_area'];
            $temp['garden_area']=$row['garden_area'];
            $temp['total_area']=$row['total_area'];
            $temp['created_at']=$row['created_at'];
            $temp['country']= $row['country'];
            $temp['address']= $row['address'];
            $temp['city']= $row['city'];
            $temp['postalcode']= $row['postalcode'];
            $temp['region']= $row['region'];
            $temp['district']= $row['district'];
            $temp['building_id']= $row['building_id'];
            $temp['latitude']= $row['latitude'];
            $temp['longitude']= $row['longitude'];
            $temp['property_title']= $row['property_title'];
            $temp['property_excerpt']= $row['property_excerpt'];
            $temp['property_description']= $row['property_description'];
        }
        return $temp;
    }
    return 0;
    
}

/* Edit property general,location, text tab*/
function updatePropertyGeneral($request){
    // echo "<pre>";
    // print_r($request);exit;
    global $conn;
    $address =mysqli_real_escape_string($conn,$request['property_address']);
    $city =mysqli_real_escape_string($conn,$request['property_city_other']);
    $region = mysqli_real_escape_string($conn,$request['property_region']);

    if($request['property_typelifetime'] != '' && isset($request['property_typelifetime'])){
        $sub_transaction_type =  $request['property_typelifetime'];
    }
    else{
        $sub_transaction_type =  $request['property_typeoffre'];   
    }
    if($request['property_typeoffre'] == 5 || $request['property_typeoffre'] == 6 || $request['property_typeoffre'] == 7 || $request['property_typeoffre'] == 8){
        $transaction_type = 5;
    }
    else{
        $transaction_type = 1;
    }
    if($request['property_type'] == 1 || $request['property_type'] == 2 || $request['property_type'] == 3 || $request['property_type'] == 4 || $request['property_type'] == 5 || $request['property_type'] == 6){
        $property_type = 1;
    }
    elseif ($request['property_type'] == 7 || $request['property_type'] == 15 || $request['property_type'] == 8 || $request['property_type'] == 9 || $request['property_type'] == 10 || $request['property_type'] == 12 || $request['property_type'] == 14 || $request['property_type'] == 13 || $request['property_type'] == 16) 
    {
        $property_type = 7;       
    }
    elseif ($request['property_type'] == 17 || $request['property_type'] == 18) 
    {
        $property_type = 12;       
    }
    elseif ($request['property_type'] == 22 || $request['property_type'] == 23 || $request['property_type'] == 24) 
    {
        $property_type = 11;       
    }
     elseif ($request['property_type'] == 19) 
    {
        $property_type = 19;       
    }
     elseif ($request['property_type'] == 20) 
    {
        $property_type = 20;       
    }
     elseif ($request['property_type'] == 21) 
    {
        $property_type = 21;       
    }
     elseif ($request['property_type'] == 25) 
    {
        $property_type = 25;       
    }
     elseif ($request['property_type'] == 26) 
    {
        $property_type = 26;       
    }
    elseif ($request['property_type'] == 27) 
    {
        $property_type = 27;       
    }
    elseif ($request['property_type'] == 28 || $request['property_type'] == 29)  
    {
        $property_type = 28;       
    }

    $last_inserted_id = '';
    $insert_query = "UPDATE tbl_property SET 
    private_note='".$request['property_privatenote']."',
    reference='".$request['reference']."',
    transaction_type='".$transaction_type."',
    sub_transaction_type='".$sub_transaction_type."',
    price = '".$request['property_prifix']."',
    monthly_rent = '".$request['property_monthlyrent']."',
    location_rental = '".$request['property_locationrent']."',
    no_retrocession = '".$request['property_noretrocession']."',
    monthly_service_charge = '".$request['property_monthlycharge']."',
    annual_costs = '".$request['property_annualcost']."',
    release_date = '".$request['property_releasedate']."',
    sole_agent = '".$request['property_soleagent']."',
    collaboration_with = '".$request['property_collaboration']."',
    property_type='".$property_type."',
    sub_property_type='".$request['subtypeproperty']."',
    mixed_use = '".$request['property_mixeduse']."',
    furnished = '".$request['property_furnished']."',
    num_rooms ='".$request['numrooms']."',
    num_bedrooms='".$request['numbedrooms']."',
    num_bathrooms='".$request['numsbathrooms']."',
    num_parking='".$request['numparking']."',
    num_box='".$request['numbox']."',
    num_cellars='".$request['numcaves']."',
    floor = '".$request['property_floor']."',
    interesting_view = '".$request['property_intrestingview']."',
    state = '".$request['property_state']."',
    living_area = '".$request['property_livingarea']."',
    terrace_area = '".$request['property_terracearea']."',
    land_area = '".$request['property_landarea']."',
    garden_area = '".$request['property_gardenarea']."',
    total_area = '".$request['property_totalarea']."',
    country = '".$request['property_country']."',
    address='".$address."',
    city = '".$city."',
    postalcode = '".$request['property_codepostal_other']."',
    region = '".$region."',
    district = '".$request['property_district']."',
    department = '".$request['property_department']."',
    building_id = '".$request['property_building']."',
    additional_loc_info = '".$request['property_additionallocation']."',
    latitude = '".$request['property_lat']."',
    longitude = '".$request['property_lng']."',
    property_title = '".$request['property_text_title']."',
    property_excerpt = '".$request['property_text_excerpt']."',
    property_description = '".$request['property_text_description']."',
    updated_at='".date("Y-m-d H:i:s")."' WHERE property_id = '".$request['propertyid']."'";
    //print_r($insert_query);exit;
    //mysqli_query($conn, $insert_query);
    //$last_inserted_id = mysqli_insert_id($conn);
    
    if(mysqli_query($conn, $insert_query)){
        return 1;
    }else{
        return 0;
    }
}

/* List all property */
function allProperty(){
    global $conn;
    $query = "SELECT * from tbl_property WHERE is_delete='0' AND is_draft='1'";
    $res = mysqli_query($conn, $query);
    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        $i = 0;
        while ($row = mysqli_fetch_array($res)) {
            $temp[$i]['property_id'] = $row['property_id'];
            $temp[$i]['updated_at']= $row['updated_at'];
            $temp[$i]['reference']= $row['reference'];
            $temp[$i]['transaction_type']= $row['transaction_type'];
            $temp[$i]['address']= $row['address'];
            $temp[$i]['price']= $row['price'];
            $temp[$i]['num_rooms']= $row['num_rooms'];
            $temp[$i]['building_id']= $row['building_id'];
            $temp[$i]['total_area']= $row['total_area'];
            $temp[$i]['property_type'] = $row['property_type'];
            $temp[$i]['no_retrocession'] = $row['no_retrocession'];
            $temp[$i]['floor'] = $row['floor'];
            $temp[$i]['living_area'] = $row['living_area'];
            $temp[$i]['release_date'] = $row['release_date'];
            $temp[$i]['created_at'] = $row['created_at'];
            $i++;
        }
        return $temp;
    }
    return 0;
}

/* get single property */
/* view singel Building */
function singleProperty($property_id){
    global $conn;
    $query = "SELECT * from tbl_property WHERE is_delete='0' AND property_id = '".$property_id."'";
    $res = mysqli_query($conn, $query);

    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        
        while ($row = mysqli_fetch_array($res)) {
            $temp['property_id']= $row['property_id'];
            $temp['property_title']= $row['property_title'];
            $temp['property_excerpt']= $row['property_excerpt'];
            $temp['reference']= $row['reference'];
            $temp['property_type']= $row['property_type'];
            $temp['transaction_type']= $row['transaction_type'];
            $temp['price']= $row['price'];
            $temp['country']= $row['country'];
            $temp['district']= $row['district'];
            $temp['building_id']= $row['building_id'];
            $temp['num_rooms']= $row['num_rooms'];
            $temp['num_parking']= $row['num_parking'];
            $temp['living_area']= $row['living_area'];
            $temp['total_area']= $row['total_area'];
            $temp['property_description']= $row['property_description'];
            $temp['created_at']= $row['created_at'];
            $temp['updated_at']= $row['updated_at'];
        }
        return $temp;
    }
    return 0;
}

/* Add building */
function addBuilding($request){
    global $conn;
    
    $address =mysqli_real_escape_string($conn,$request['address']);
    if(isset($request['city']) && $request['city'] != ''){
        $city =mysqli_real_escape_string($conn,$request['city']);
    }
    else{
        $city =mysqli_real_escape_string($conn,$request['city_other']);
    }
    if(isset($request['district']) && $request['district'] != ''){
        $district =mysqli_real_escape_string($conn,$request['district']);
    }
    else{
        $district =mysqli_real_escape_string($conn,$request['district_other']);
    }

    
    $last_inserted_id = '';
    $insert_query = "INSERT INTO tbl_building SET user_id='".$_SESSION['user_id']."',building_name='".$request['building_name']."', country='".$request['country']."', address='".$address."', city='".$city."',district = '".$district."',latitude='".$request['latitude']."',longitude = '".$request['longitude']."',floors = '".$request['floors']."',ranking = '".$request['ranking']."',height = '".$request['height']."',swimming_pool= '".$request['swimming_pool']."',created_at='".date("Y-m-d H:i:s")."' ";
    //print_r($insert_query);
    mysqli_query($conn, $insert_query);
    $last_inserted_id = mysqli_insert_id($conn);
    
    if($last_inserted_id > 0){
        return $last_inserted_id;
    }else{
        return 0;
    }
}

/* view all Building */
function allBuilding(){
    global $conn;
    $query = "SELECT * from tbl_building WHERE is_delete='0' AND user_id != '".$_SESSION['user_id']."'";
    $res = mysqli_query($conn, $query);
    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        $i = 0;
        while ($row = mysqli_fetch_array($res)) {
            $temp[$i]['building_id']= $row['building_id'];
            $temp[$i]['building_name']= $row['building_name'];
            $temp[$i]['address']= $row['address'];
            $temp[$i]['district']= $row['district'];
            $i++;
        }
        return $temp;
    }
    return 0;
    
}

/* view My Building */
function myBuilding(){
    global $conn;
    $query = "SELECT * from tbl_building WHERE is_delete='0' AND user_id = '".$_SESSION['user_id']."'";
    $res = mysqli_query($conn, $query);

    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        $i = 0;
        while ($row = mysqli_fetch_array($res)) {
            $temp[$i]['building_id']= $row['building_id'];
            $temp[$i]['building_name']= $row['building_name'];
            $temp[$i]['address']= $row['address'];
            $temp[$i]['district']= $row['district'];
            $i++;
        }
        return $temp;
    }
    return 0;
    
}

/* view singel Building */
function singelBuilding($building_id){
    global $conn;
    $query = "SELECT * from tbl_building WHERE is_delete='0' AND building_id = '".$building_id."'";
    $res = mysqli_query($conn, $query);

    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        
        while ($row = mysqli_fetch_array($res)) {
            $temp['building_id']= $row['building_id'];
            $temp['building_name']= $row['building_name'];
            $temp['address']= $row['address'];
            $temp['district']= $row['district'];
            $temp['country']= $row['country'];
            $temp['city']= $row['city'];
            $temp['region']= $row['region'];
            $temp['latitude']= $row['latitude'];
            $temp['longitude']= $row['longitude'];
            $temp['ranking']= $row['ranking'];
            $temp['floors']= $row['floors'];
            $temp['height']= $row['height'];
            $temp['swimming_pool']= $row['swimming_pool'];
            
        }
        return $temp;
    }
    return 0;
}

/** Get Country name from id */
function countryName($country_id){
    global $conn;
    $query = "SELECT * from tbl_country WHERE country_id = '".$country_id."'";
    $res = mysqli_query($conn, $query);

    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        
        while ($row = mysqli_fetch_array($res)) {
            $temp['country_nicename']= $row['country_nicename'];
            $temp['iso_code']= $row['iso_code'];
            $temp['num_code']= $row['num_code'];
        }
        return $temp;
    }
    return 0;
}

/** Get district name from id */
function cityName($city_id){
    global $conn;
    $query = "SELECT * from tbl_city WHERE city_id = '".$city_id."'";
    $res = mysqli_query($conn, $query);

    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        
        while ($row = mysqli_fetch_array($res)) {
            $temp['city_name']= $row['city_name'];
            $temp['city_id']= $row['city_id'];
        }
        return $temp;
    }
    return 0;
}

/** Get district name from id */
function districtName($district_id){
    global $conn;
    $query = "SELECT * from tbl_district WHERE district_id = '".$district_id."'";
    $res = mysqli_query($conn, $query);

    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        
        while ($row = mysqli_fetch_array($res)) {
            $temp['district_name']= $row['district_name'];
            $temp['district_id']= $row['district_id'];
        }
        return $temp;
    }
    return 0;
}

// Get a building name
function buildingName($building_id){
    global $conn;
    $query = "SELECT * from tbl_building WHERE building_id = '".$building_id."'";
    $res = mysqli_query($conn, $query);

    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        
        while ($row = mysqli_fetch_array($res)) {
            $building_name= $row['building_name'];
            //$temp['district_id']= $row['district_id'];
        }
        return $building_name;
    }
    return '-';
}

// Get a property type name
function propertyType($property_type_id){
    global $conn;
    $query = "SELECT * from tbl_property_type WHERE property_type_id = '".$property_type_id."'";
    $res = mysqli_query($conn, $query);

    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        
        while ($row = mysqli_fetch_array($res)) {
            $property_type_name= $row['property_type_name'];
            //$temp['district_id']= $row['district_id'];
        }
        return $property_type_name;
    }
}

// Get a transection type name
function transactionType($transaction_type_id){
    global $conn;
    $query = "SELECT * from tbl_transaction_type WHERE transaction_type_id = '".$transaction_type_id."'";
    $res = mysqli_query($conn, $query);

    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        
        while ($row = mysqli_fetch_array($res)) {
            $transaction_type_name= $row['transaction_type_name'];
            //$temp['district_id']= $row['district_id'];
        }
        return $transaction_type_name;
    }
}

/* Update a building */
function upadateBuilding($request){
    global $conn;
    
    $address =mysqli_real_escape_string($conn,$request['address']);
    if(isset($request['city']) && $request['city'] != ''){
        $city =mysqli_real_escape_string($conn,$request['city']);
    }
    else{
        $city =mysqli_real_escape_string($conn,$request['city_other']);
    }
    if(isset($request['district']) && $request['district'] != ''){
        $district =mysqli_real_escape_string($conn,$request['district']);
    }
    else{
        $district =mysqli_real_escape_string($conn,$request['district_other']);
    }
    
    //$last_inserted_id = '';
    $insert_query = "UPDATE tbl_building SET building_name='".$request['building_name']."', country='".$request['country']."', address='".$address."', city='".$city."',district = '".$request['district']."',latitude='".$request['latitude']."',longitude = '".$request['longitude']."',floors = '".$request['floors']."',ranking = '".$request['ranking']."',height = '".$request['height']."',swimming_pool= '".$request['swimming_pool']."',created_at='".date("Y-m-d H:i:s")."' WHERE building_id = '".$request['building_id']."' ";
    //print_r($insert_query);
       
    if(mysqli_query($conn, $insert_query)){
        return 1;
    }else{
        return 0;
    }
}

/* delete a Building */
function removeBuildingData($building_id) {
    global $conn;
    $delete_query = "DELETE FROM tbl_building WHERE building_id = " . $building_id;
    $delete_res = mysqli_query($conn, $delete_query);

    if ($delete_res) {
        return 1;
    }
    return 0;
}

function deleteProperty($property_id) {
    global $conn;
    $delete_query = "DELETE FROM tbl_property WHERE property_id = " . $property_id;
    $delete_res = mysqli_query($conn, $delete_query);

    if ($delete_res) {
        return 1;
    }
    return 0;
}

/* Offmarket Property */
function OffmarketProperty($property_id) {
    global $conn;
    $update_query = "UPDATE tbl_property SET is_draft='0', is_offmarket='1' WHERE property_id = ".$property_id."";
    $update_res = mysqli_query($conn, $update_query);

    if ($update_res) {
        return 1;
    }
    return 0;
}

/* Public Property */
function publicProperty($property_id) {
    global $conn;
    $update_query = "UPDATE tbl_property SET is_draft='0', is_public='1' WHERE property_id = ".$property_id."";
    $update_res = mysqli_query($conn, $update_query);

    if ($update_res) {
        return 1;
    }
    return 0;
}

function archiveProperty($property_id) {
    global $conn;
    $update_query = "UPDATE tbl_property SET is_draft='0', is_archive='1' WHERE property_id = ".$property_id."";
    $update_res = mysqli_query($conn, $update_query);

    if ($update_res) {
        return 1;
    }
    return 0;
}

/* Add Contact */
function addContact($newfilename, $newfilenameadd) {
    global $conn;
    $last_inserted_id = '';
    extract($_POST);
    $langspoken = array();
    $langspoken = $_POST['tLanguageSpoken'];
    $dob = date("Y-m-d", strtotime($_POST['dDateofbirth']));
    $insert_query = "INSERT INTO tbl_contact SET vContactType='$vContactType', iUserID='$iUserID', vSalutation='$vSalutation', vFirstName = '" . addslashes($vFirstName) . "',vLastName='" . addslashes($vLastName) . "', vEmail='$vEmail', vPhone = '$vPhone', dDateofbirth='$dob', vCitizenship='" . addslashes($vCitizenship) . "', vAddress = '" . addslashes($vAddress) . "', vCity='" . addslashes($vCity) . "',country_id='$country_id',tUploadID='$newfilename',tAddressProof='$newfilenameadd', created_at=NOW()";
    mysqli_query($conn, $insert_query);
    $last_inserted_id = mysqli_insert_id($conn);
    foreach ($langspoken as $value) {
        $insert_lang = "INSERT INTO tbl_language_spoken SET iContactID='$last_inserted_id',vLangaugeSpoken='$value'";
        mysqli_query($conn, $insert_lang);
    }
    return $last_inserted_id;
}

/* Edit Contact */
function editContact($newfilename, $newfilenameadd, $contactid) {    
    global $conn;      
    extract($_POST);
    
    if ($newfilename != "" && $newfilenameadd != "") {          
        $select_data = "SELECT tUploadID,tAddressProof FROM tbl_contact WHERE iContactID =" . $contactid;
        $result = mysqli_query($conn, $select_data);
        
        if (mysqli_num_rows($result) > 0) {
            $row = mysqli_fetch_assoc($result);           
            unlink(ID_PROOF_IMAGE_PATH . $row['tUploadID']);
            unlink(ID_PROOF_IMAGE_THUMB_PATH . $row['tUploadID']); 
            unlink(ADD_PROOF_IMAGE_PATH . $row['tAddressProof']);
            unlink(ADD_PROOF_IMAGE_THUMB_PATH . $row['tAddressProof']);
        }
    }
    else if ($newfilename != "") {        
        $select_data = "SELECT tUploadID FROM tbl_contact WHERE iContactID =" . $contactid;
        $result = mysqli_query($conn, $select_data);

        if (mysqli_num_rows($result) > 0) {
            $row = mysqli_fetch_assoc($result);            
            unlink(ID_PROOF_IMAGE_PATH . $row['tUploadID']);
            unlink(ID_PROOF_IMAGE_THUMB_PATH . $row['tUploadID']);            
        }
    }else if($newfilenameadd != ""){                    
        $select_data = "SELECT tAddressProof FROM tbl_contact WHERE iContactID =" . $contactid;
        $result = mysqli_query($conn, $select_data);

        if (mysqli_num_rows($result) > 0) {
            $row = mysqli_fetch_assoc($result);          
            unlink(ADD_PROOF_IMAGE_PATH . $row['tAddressProof']);
            unlink(ADD_PROOF_IMAGE_THUMB_PATH . $row['tAddressProof']);
        }
    }
    
    if($eActive == 'on'){
        $eActive = 0;
    }else{
        $eActive = 1;
    }
    $langspoken = array();
    $langspoken = $_POST['tLanguageSpoken'];
    
    $dob = date("Y-m-d", strtotime($dDateofbirth));   
    
     if ((isset($newfilename) && $newfilename != "") && (isset($newfilenameadd) && $newfilenameadd != "")) {
         $update_query = "UPDATE tbl_contact SET vContactType='$vContactType', iUserID='$iUserID', vSalutation='$vSalutation', vFirstName = '" . addslashes($vFirstName) . "',vLastName='" . addslashes($vLastName) . "', vEmail='$vEmail', vPhone = '$vPhone', dDateofbirth='$dob', vCitizenship='" . addslashes($vCitizenship) . "', vAddress = '" . addslashes($vAddress) . "', vCity='" . addslashes($vCity) . "',country_id='$country_id',tUploadID='$newfilename',tAddressProof='$newfilenameadd',eActive='$eActive' WHERE iContactID = $contactid";
     }else if(isset($newfilename) && $newfilename != ""){
         $update_query = "UPDATE tbl_contact SET vContactType='$vContactType', iUserID='$iUserID', vSalutation='$vSalutation', vFirstName = '" . addslashes($vFirstName) . "',vLastName='" . addslashes($vLastName) . "', vEmail='$vEmail', vPhone = '$vPhone', dDateofbirth='$dob', vCitizenship='" . addslashes($vCitizenship) . "', vAddress = '" . addslashes($vAddress) . "', vCity='" . addslashes($vCity) . "',country_id='$country_id',tUploadID='$newfilename',eActive='$eActive' WHERE iContactID = $contactid";
     }else if(isset($newfilenameadd) && $newfilenameadd != ""){
         $update_query = "UPDATE tbl_contact SET vContactType='$vContactType', iUserID='$iUserID', vSalutation='$vSalutation', vFirstName = '" . addslashes($vFirstName) . "',vLastName='" . addslashes($vLastName) . "', vEmail='$vEmail', vPhone = '$vPhone', dDateofbirth='$dob', vCitizenship='" . addslashes($vCitizenship) . "', vAddress = '" . addslashes($vAddress) . "', vCity='" . addslashes($vCity) . "',country_id='$country_id',tAddressProof='$newfilenameadd',eActive='$eActive' WHERE iContactID = $contactid";
     }else{
         $update_query = "UPDATE tbl_contact SET vContactType='$vContactType', iUserID='$iUserID', vSalutation='$vSalutation', vFirstName = '" . addslashes($vFirstName) . "',vLastName='" . addslashes($vLastName) . "', vEmail='$vEmail', vPhone = '$vPhone', dDateofbirth='$dob', vCitizenship='" . addslashes($vCitizenship) . "', vAddress = '" . addslashes($vAddress) . "', vCity='" . addslashes($vCity) . "',country_id='$country_id',eActive='$eActive' WHERE iContactID = $contactid";
     }
    
    $update_res = mysqli_query($conn, $update_query);

    $delete_query = "DELETE FROM tbl_language_spoken WHERE iContactID='$contactid'";
    mysqli_query($conn, $delete_query);

    foreach ($langspoken as $value) {
        $insert_lang = "INSERT INTO tbl_language_spoken SET iContactID='$contactid',vLangaugeSpoken='$value'";
        mysqli_query($conn, $insert_lang);
    }
    return $update_res;
}

/* View Contact */
function viewallcontacts() {
    global $conn;
    $query = "SELECT tc.*,tu.user_id, tu.first_name,tu.last_name,tu.email,tu.is_delete from tbl_contact as tc LEFT JOIN tbl_user as tu ON tu.user_id = tc.iUserID WHERE tu.is_delete = '0' AND tu.is_active = '1' ";
    $res = mysqli_query($conn, $query);
    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        $i = 0;
        while ($row = mysqli_fetch_array($res)) {
            $temp[$i]['iContactID'] = $row['iContactID'];
            $temp[$i]['vContactType'] = $row['vContactType'];
            $temp[$i]['vFirstName'] = $row['vFirstName'];
            $temp[$i]['vLastName'] = $row['vLastName'];
            $temp[$i]['vCity'] = $row['vCity'];
            $temp[$i]['vPhone'] = $row['vPhone'];
            $temp[$i]['vEmail'] = $row['vEmail'];
            $temp[$i]['eActive'] = $row['eActive'];
            $i++;
        }
        return $temp;
    }else{
        return $temp;
    }        
}

/* Single View Contact */
function singleContact($contactid) {
    global $conn;
    $query = "SELECT tc.*,tco.country_id,tco.country_nicename,tu.user_id, tu.first_name,tu.last_name,tu.email,tu.is_delete from tbl_contact as tc LEFT JOIN tbl_user as tu ON tu.user_id = tc.iUserID LEFT JOIN tbl_country as tco ON tco.country_id = tc.country_id WHERE tu.is_delete = '0' AND tu.is_active = '1' AND tc.iContactID = '" . $contactid . "' ";    
    $res = mysqli_query($conn, $query);
    $temp = array();
    if (mysqli_num_rows($res) > 0) {
        while ($row = mysqli_fetch_array($res)) {
            $temp['iContactID'] = $row['iContactID'];
            $temp['vContactType'] = $row['vContactType'];
            $temp['first_name'] = $row['first_name'];
            $temp['last_name'] = $row['last_name'];
            $temp['vFirstName'] = $row['vFirstName'];
            $temp['vLastName'] = $row['vLastName'];
            $temp['vCity'] = $row['vCity'];
            $temp['vPhone'] = $row['vPhone'];
            $temp['vEmail'] = $row['vEmail'];
            $temp['eActive'] = $row['eActive'];
            $temp['country_nicename'] = $row['country_nicename'];
            $temp['dDateofbirth'] = $row['dDateofbirth'];
            $temp['vCitizenship'] = $row['vCitizenship'];
            $temp['vAddress'] = $row['vAddress'];
            $temp['tUploadID'] = $row['tUploadID'];
            $temp['tAddressProof'] = $row['tAddressProof'];
        }
        return $temp;
    } else {
        return $temp;
    }
}

/* Get Contact Detail */
function getContact($cid){
     global $conn;
    $query = "SELECT tc.*,tco.country_id,tco.country_nicename,tu.user_id, tu.first_name,tu.last_name,tu.email,tu.is_delete from tbl_contact as tc LEFT JOIN tbl_user as tu ON tu.user_id = tc.iUserID LEFT JOIN tbl_country as tco ON tco.country_id = tc.country_id WHERE tu.is_delete = '0' AND tu.is_active = '1' AND tc.iContactID = '".$cid."' ";        
    $res = mysqli_query($conn, $query);
    
    $query1 = "SELECT * FROM tbl_language_spoken WHERE iContactID='".$cid."'"; 
    $result = mysqli_query($conn, $query1);
    $langarry = array();
     if (mysqli_num_rows($result) > 0) {        
        while($rowdata = mysqli_fetch_array($result)){
            $langarry[] = $rowdata['vLangaugeSpoken'];
        }         
     }
    
    $temp = array();
    if (mysqli_num_rows($res) > 0) {        
        while ($row = mysqli_fetch_array($res)) {
            $temp['iContactID'] = $row['iContactID'];
            $temp['vContactType'] = $row['vContactType'];
            $temp['user_id'] = $row['user_id'];
            $temp['vSalutation'] = $row['vSalutation'];
            $temp['vFirstName'] = $row['vFirstName'];
            $temp['vLastName'] = $row['vLastName'];
            $temp['vEmail'] = $row['vEmail'];
            $temp['vPhone'] = $row['vPhone'];
            $temp['dDateofbirth'] = $row['dDateofbirth'];
            $temp['vCitizenship'] = $row['vCitizenship'];
            $temp['vAddress'] = $row['vAddress'];
            $temp['vCity'] = $row['vCity'];
            $temp['country_id'] = $row['country_id'];
            $temp['country_nicename'] = $row['country_nicename']; 
            $temp['tUploadID'] = $row['tUploadID']; 
            $temp['tAddressProof'] = $row['tAddressProof'];             
            $temp['vLangaugeSpoken'] = $langarry;             
            $temp['eActive'] = $row['eActive'];                                   
        }        
        return $temp;
    }else{
        return $temp;
    }   
}

function getuserlistfromtype($contype) {
    global $conn;
    $query = "SELECT * FROM tbl_user_type WHERE user_type LIKE " . "'$contype'";
    $res = mysqli_query($conn, $query);
    $rowcount = mysqli_num_rows($res);
    $rdata = array();
    if ($rowcount > 0) {
        $rowC = mysqli_fetch_assoc($res);
        $query2 = "SELECT user_id,first_name,last_name FROM tbl_user WHERE user_type_id = '" . $rowC['user_type_id'] . "'";
        $result = mysqli_query($conn, $query2);
        return $result;
    } else {
        return $rdata;
    }
}

function getspokenlangbycontactid($cid) {
    global $conn;
    $query1 = "SELECT * FROM tbl_language_spoken WHERE iContactID='" . $cid . "'";
    $result = mysqli_query($conn, $query1);
    $langarry = array();
    if (mysqli_num_rows($result) > 0) {
        while ($rowdata = mysqli_fetch_array($result)) {
            $langarry[] = $rowdata['vLangaugeSpoken'];
        }
        return $langarry;
    }
}

function addFolderName($foldername){
    global $conn;
    $last_inserted_id = '';

    $insert_query = "INSERT INTO tbl_folder_list SET first_name='$first_name', last_name='$last_name', email='$email', password = '$password',user_type_id='$user_type',is_active = '1',is_delete='0', created_at=NOW(),updated_at=NOW()";

    mysqli_query($conn, $insert_query);
    $last_inserted_id = mysqli_insert_id($conn);
    return $last_inserted_id;
}