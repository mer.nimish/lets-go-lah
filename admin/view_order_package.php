<?php
if(!isset($_COOKIE['login'])){
    header('location: index.php');
}
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;

include('header.php');
include('left_sidebar.php');
$userslist = getUserOrderList();

?>

<div class="page-wrapper">
    <div class="container-fluid ">	        
        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">View Order List</h5>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Order</a></li>
                    <li class="active"><span>Order List</span></li>
                </ol>
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-sm-12">
                <div class="response_message"></div>
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Users Order List</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="">
                                    <table id="myTable1" class="table table-hover display pb-30" >
                                        <thead>
                                            <tr>
                                                <th>Sr No.</th>
                                                <th>User Name</th>
                                                <th>Email</th>
                                                <th>Package Name</th>
                                                <th>Budget Type</th>
                                                <th>Start Date</th> 
                                                <th>End Date</th>
                                                <th>Price</th>
                                                <th>Payment</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            if (!empty($userslist)) {
                                                $count=1;
                                                $res = mysqli_query($conn, $userslist);
                                                while ($alluserlist = mysqli_fetch_assoc($res)) {
                                                    ?>
                                                    <tr id="row_<?=$alluserlist["order_id"]?>">
                                                        <td style="font-size: 20px !important;font-weight: 600;"><?= $count;?></td>
                                                        <td><?= $alluserlist['first_name'] . ' ' . $alluserlist['last_name'] ?></td>
                                                        <td><?= $alluserlist['email'] ?></td>
                                                        <td><?= $alluserlist['pkg_name'] ?></td>
                                                        <td><?= $alluserlist['budget_type'] ?></td>
                                                        <td><?= date("d/m/Y", strtotime($alluserlist['pkg_start_date'])); ?></td>
                                                        <td><?= date("d/m/Y", strtotime($alluserlist['pkg_end_date'])); ?></td>
                                                        <td><?= $alluserlist['price'] ?></td>
                                                        <td><?= $alluserlist['payment'] ?></td> 
                                                    </tr>
                                                <?php } } ?>
                                        </tbody>                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>	
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.order').addClass('active');
                $('.order').mouseover(function(){
                    $('.order').addClass('active');
                    $('.dashboard,.user,.budget,.package,.gallery').removeClass('active');
                });
                $('.dashboard').mouseover(function(){
                    $('.dashboard').addClass('active');
                    $('.user,.budget,.package').removeClass('active');
                });
                $('.user').mouseover(function(){
                    $('.user').addClass('active');
                    $('.dashboard,.budget,.package').removeClass('active');
                }); 
                $('.budget').mouseover(function(){
                    $('.budget').addClass('active');
                    $('.dashboard,.user,.package').removeClass('active');
                });
                $('.package').mouseover(function(){
                    $('.package').addClass('active');
                    $('.dashboard,.user,.budget,.gallery').removeClass('active');
                });
                $('.gallery').mouseover(function(){
                    $('.gallery').addClass('active');
                    $('.dashboard,.user,.package').removeClass('active');
                });
            });
        </script>
        <?php include('footer.php'); ?>
        