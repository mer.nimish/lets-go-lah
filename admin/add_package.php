<?php
/* --- Start Check Set Cookie or Not --- */    
if(!isset($_COOKIE['login'])){
    header('location: index.php');
}
/* --- End Check Set Cookie or Not --- */    
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;

// $Budgetlist = getBudgetlist();

if (isset($_POST['createpkgbtn'])) {
    /* --- Start File Uploading --- */
    $pkg_title       = $_POST['pkg_title'];
    $pkg_start_price = $_POST['pkg_start_price'];
    $pkg_end_price   = $_POST['pkg_end_price'];
    $pkg_day_price   = $_POST['pkg_day_price'];
    $pkg_night_price = $_POST['pkg_night_price'];   
    $content         = $_POST['content'];
    $pkg_incl        = $_POST['pkg_incl'];

    $name       =   $_FILES['pkg_img']['name'];
    $tmp_name   =   $_FILES['pkg_img']['tmp_name'];
    $size       =   $_FILES['pkg_img']['size'];
    
    $name_arr   =   explode('.', $name);
    $first = current($name_arr).'_'.uniqid();
    $extension  =   strtolower(end($name_arr));
    $imgname =  $first.'.'.$extension;
    $uploadPath =   $_SERVER['DOCUMENT_ROOT'].'/lets-go-lah/img/package/'.$imgname;

    if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'bmp'){
        if ($size <= 2000000) {
            move_uploaded_file($tmp_name, $uploadPath);
    /* --- End File Uploading --- */
            
    /* --- Start Insert data --- */
            $last_inserted_id = '';
            $last_inserted_id = addPackage($pkg_title, $pkg_start_price, $pkg_end_price,$content,$pkg_incl,$imgname);
    /* --- End Insert data --- */
        }
    }

    /* --- Start Data check Inserted or Not --- */
    if ($last_inserted_id > 0 && $last_inserted_id != '') {
        /*addPkgBudget($last_inserted_id,$_POST['budget_id'],$_POST['night_price']);*/
        $message = '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Yay! Your form has been sent successfully.</p> 
						<div class="clearfix"></div>
					</div>';

    } else {
        $message = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.</p>
						<div class="clearfix"></div>
					</div>';
    }
    /* --- End Data check Inserted or Not --- */
}

include('header.php');
include('left_sidebar.php');
?>

<div class="page-wrapper">
    <div class="container-fluid ">	
        <div class="col-md-12">
            <?php if (isset($successMsg)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left"><?= $successMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorEmailMsg)) { ?>
                <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-alert-circle-o pr-15 pull-left"></i><p class="pull-left"><?= $errorEmailMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorOtherMsg)) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left"><?= $errorOtherMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
        </div>

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Create New Package</h5>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Package</a></li>
                    <li class="active"><span>Create Package</span></li>
                </ol>
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Package Form</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-8">
                                    <?php echo $message; ?>
                                    <div class="form-wrap">
                                        <form data-toggle="validator" name="createpkgform" id="createpkgform" method="post" action="#" role="form" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="pkg_title" class="control-label mb-10">Package Title</label>
                                                <input type="text" class="form-control" name="pkg_title" id="pkg_title" placeholder="Package Title" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="pkg_start_price" class="control-label mb-10">Package Start Price</label>
                                                <input type="number" class="form-control" name="pkg_start_price" id="pkg_start_price" placeholder="Package Start Price" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="pkg_end_price" class="control-label mb-10">Package End Price</label>
                                                <input type="number" class="form-control" name="pkg_end_price" id="pkg_end_price" placeholder="Package End Price" required>
                                            </div>
                                            <!-- <div class="form-group">
                                                <div class="col-md-4">
                                                    <label class="control-label mb-10">Budget Type</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <label class="control-label mb-10">Night Price</label>
                                                </div>
                                            </div>  -->
                                             <?php
                                            // if (!empty($Budgetlist)) {
                                            //     while ($data = mysqli_fetch_assoc($Budgetlist)) {
                                            ?>
                                           
                                            <!-- <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="hidden" class="form-control" name="budget_id[]" value="<?php// echo $data['id']?>" placeholder="<?php //echo $data['budget_title'].' Price'?>" required> <?php //echo $data['budget_id']?>  
                                                        <span style="margin-left: 30px">
                                                            <?php //echo $data['budget_title']?>
                                                        </span>
                                                     </div>
                                                     <div class="col-md-3">
                                                        <input type="number" class="form-control" name="night_price[]" id="night_price" placeholder="<?php //echo $data['budget_title'].' Price'?>" required>
                                                     </div>
                                                     <div class="col-md-5"></div>
                                                </div>
                                             </div>    -->
                                            <?php //} }?>
                                            <div class="form-group">
                                                <label for="pkg_img" class="control-label mb-10">Package Image</label>
                                                <input type="file" name="pkg_img" id="pkg_img" required>
                                                <span class="error" style="color: red !important;"> </span>
                                            </div>
                                            <div class="img-upload-wrap gallery">
                                                <img src="" id="pkg_img_tag"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="content" class="control-label mb-10">Package Description</label>
                                                <textarea name="content" id="editor">
                                                    &lt;p&gt; &lt;/p&gt;
                                                </textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="pkg_incl" class="control-label mb-10">Package Inclusion</label>
                                                <textarea name="pkg_incl" id="pkg_incl">
                                                    &lt;p&gt; &lt;/p&gt;
                                                </textarea>
                                            </div>
                                            <div class="form-group mb-0">
                                                <input type="submit" value="Submit" name="createpkgbtn" id="createpkgbtn" class="btn btn-success btn-anim">
                                            </div>                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style type="text/css">
            input[type=number]::-webkit-inner-spin-button, 
            input[type=number]::-webkit-outer-spin-button { 
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                margin: 0; 
            }
        </style>

        <?php include('footer.php'); ?>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script>
        /* --- Start CHEditor --- */
            
            ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
            ClassicEditor
            .create( document.querySelector( '#pkg_incl' ) )
            .catch( error => {
                console.error( error );
            } );

        /* --- End CHEditor --- */
            $(document).ready(function() {

            /* --- Start View Selected File --- */
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#pkg_img_tag').attr('src', e.target.result).height(150).width(150);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                $("#pkg_img").change(function(){
                    readURL(this);
                });
            /* --- End View Selected File --- */
            /* --- Start File Validation --- */
            $("#pkg_img").change(function () {
                var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
                $('.error').html('')
                if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    alert("Only formats are allowed : "+fileExtension.join(', '));
                    $('.error').html('Valid only jpeg, jpg, png, gif and bmp file.')
                    var file = document.getElementById("pkg_img");
                        file.value = file.defaultValue;
                }
            });
            /* --- End File Validation --- */
            /* --- Start Spinner of Arrow key disabled --- */    
                $("input[type=number]").on("focus", function() {
                    $(this).on("keydown", function(event) {
                        if (event.keyCode === 38 || event.keyCode === 40) {
                            event.preventDefault();
                        }
                    });
                });
            /* --- End Spinner of Arrow key disabled --- */    

            /* --- Start Side Left Bar Active/Inactive Menu --- */    

                $('.package').addClass('active');
                $('#package').addClass('collapse in');
                $('.Apackage').css('color', '#000');
                $('.Apackage').css('background-color', '#0fc5bb');
                $('.Vpackage').css('color', '#878787');

                $('.package').mouseover(function(){
                    $('.package').addClass('active');
                    $('.dashboard,.user,.budget,.order,.gallery').removeClass('active');
                });

                $('.dashboard').mouseover(function(){
                    $('.dashboard').addClass('active');
                    $('.user,.budget,.order,.gallery').removeClass('active');
                });
                $('.user').mouseover(function(){
                    $('.user').addClass('active');
                    $('.dashboard,.budget,.order,.gallery').removeClass('active');
                }); 
                $('.budget').mouseover(function(){
                    $('.budget').addClass('active');
                    $('.dashboard,.user,.order,.gallery').removeClass('active');
                });
                $('.order').mouseover(function(){
                    $('.order').addClass('active');
                    $('.dashboard,.user,.budget,.gallery').removeClass('active');
                });
                $('.gallery').mouseover(function(){
                    $('.gallery').addClass('active');
                    $('.dashboard,.user,.order').removeClass('active');
                });
            /* --- End Side Left Bar Active/Inactive Menu --- */    

            });
            
        </script>