<?php
if(!isset($_COOKIE['login'])){
    header('location: index.php');
}
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;

$id = $_REQUEST['id'];
$user = getUser($id);
$res = mysqli_query($conn, $user);

if (isset($_POST['updateuserbtn'])) {

    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $email = $_POST['email'];
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    $user_type = 1;
    $status = $_POST['status'];
    $oldimg = $_POST['oldimg'];

    $name       =   $_FILES['user_img']['name'];
    $tmp_name   =   $_FILES['user_img']['tmp_name'];
    $size       =   $_FILES['user_img']['size'];
    
    $name_arr   =   explode('.', $name);
    $first = current($name_arr).'_'.uniqid();
    $extension  =   strtolower(end($name_arr));
    $imgname =  $first.'.'.$extension;
    $uploadPath =   $_SERVER['DOCUMENT_ROOT'].'/lets-go-lah/img/users/'.$imgname;
    if($name){
        if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'bmp'){
            if ($size <= 2000000) {
                $dirpath =   $_SERVER['DOCUMENT_ROOT'].'/lets-go-lah/img/users/';
                $dir = opendir($dirpath);
                while( ($file = readdir($dir)) !== false ) {
                    if ( $file == $oldimg) {
                         unlink($dirpath.'/'.$oldimg);
                    }
                }
                move_uploaded_file($tmp_name, $uploadPath);
            }
        }
        $last_inserted_id = '';
        $last_inserted_id = editUser($first_name, $last_name, $email, $password, $user_type, $status, $id, $imgname);        
    }else{
        $last_inserted_id = '';
        $last_inserted_id = editUser($first_name, $last_name, $email, $password, $user_type, $status, $id, $oldimg);
    }
    
    if ($last_inserted_id > 0 && $last_inserted_id != '') {
        $message = '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Yay! Your form has been sent successfully.</p> 
						<div class="clearfix"></div>
					</div>';
                    header('location: view_user.php');
    } else {
        $message = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.</p>
						<div class="clearfix"></div>
					</div>';
    }
}
$allusertypeslist = getusertypes();
include('header.php');
include('left_sidebar.php');
?>

<div class="page-wrapper">
    <div class="container-fluid">	
        <div class="col-md-12">
            <?php if (isset($successMsg)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left"><?= $successMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorEmailMsg)) { ?>
                <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-alert-circle-o pr-15 pull-left"></i><p class="pull-left"><?= $errorEmailMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorOtherMsg)) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left"><?= $errorOtherMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
        </div>

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Update User</h5>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">User</a></li>
                    <li class="active"><span>Update User</span></li>
                </ol>
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">User Form</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-8">
                                    <?php echo $message; ?>
                                    <div class="form-wrap">
                                        <form data-toggle="validator" name="edituserform" id="edituserform" method="post" action="#" role="form" enctype="multipart/form-data">
                                    
                                    <?php  while ($row = mysqli_fetch_assoc($res)) { ?>

                                        <input type="hidden" name="oldimg" value="<?= $row['user_img']; ?>">
                                            <div class="form-group">
                                                <label for="first_name" class="control-label mb-10">First Name</label>
                                                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" value="<?php echo $row['first_name'];?>" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="LastName" class="control-label mb-10">Last Name</label>
                                                <input type="text" class="form-control" name="last_name" id="LastName" placeholder="Last Name" value="<?php echo $row['last_name'];?>" required>
                                            </div>

                                            <div class="form-group">
                                                <label for="email" class="control-label mb-10">Email</label>
                                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $row['email'];?>" data-error="Bruh, that email address is invalid" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="password" class="control-label mb-10">Password</label>
                                                <div class="row">
                                                    <div class="form-group col-sm-12">
                                                        <input type="password" data-minlength="6" class="form-control" id="password" name="password" placeholder="Password" required>
                                                        <div class="help-block">Minimum of 6 characters</div>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="user_img" class="control-label mb-10">User Image</label>
                                            </div>
                                            <div class="form-group">
                                                <img src="../img/users/<?= $row['user_img'];?>" height="150" width="150" >
                                                <input type="file" class="form-control" name="user_img" id="user_img">
                                            </div>
                                            <div class="img-upload-wrap gallery">
                                                <img src="" id="user_img_tag"/>
                                            </div><br>
                                            <div class="form-group">
                                               <label for="status" class="control-label mb-10">Status</label>
                                                <select class="form-control" name="status" id="status">
                                                    <option value="1" <?php if($row['is_active'] == 1){ echo 'Selected'; }?> >Active</option>
                                                    <option value="2" <?php if($row['is_active'] == 2){ echo 'Selected'; }?> >In-Active</option>
                                                </select>
                                            </div>
                                            <?php    } ?>
                                            <div class="form-group mb-0">
                                                <input type="submit" value="Update" name="updateuserbtn" id="updateuserbtn" class="btn btn-success btn-anim">
                                            </div>                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style type="text/css">
            input[type=number]::-webkit-inner-spin-button, 
            input[type=number]::-webkit-outer-spin-button { 
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                margin: 0; 
            }
        </style>

        <?php include('footer.php'); ?>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $('form[id="createuserform"]').validate({
                rules: {
                    email: {                                               
                        remote: {
                            url: "ajax/checkemail.php",
                            type: "post"
                        }
                    }
                },
                messages: {
                    email: {                                               
                        remote: "Email already registered please use other!!"
                    }
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
            $(document).ready(function() {
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#user_img_tag').attr('src', e.target.result).height(150).width(150);
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                $("#user_img").change(function(){
                    readURL(this);
                });

                $("input[type=number]").on("focus", function() {
                    $(this).on("keydown", function(event) {
                        if (event.keyCode === 38 || event.keyCode === 40) {
                            event.preventDefault();
                        }
                    });
                });
                $('.user').addClass('active');
                $('.dashboard').mouseover(function(){
                    $('.dashboard').addClass('active');
                    $('.user').removeClass('active');
                    $('.budget').removeClass('active');
                    $('.package').removeClass('active');
                    $('.order').removeClass('active');
                });
                $('.user').mouseover(function(){
                    $('.user').addClass('active');
                    $('.dashboard,.budget,.package,.order').removeClass('active');
                }); 
                $('.budget').mouseover(function(){
                    $('.budget').addClass('active');
                    $('.dashboard,.user,.package,.order').removeClass('active');
                });
                $('.package').mouseover(function(){
                    $('.package').addClass('active');
                    $('.dashboard,.user,.budget,.order').removeClass('active');
                });
                $('.order').mouseover(function(){
                    $('.order').addClass('active');
                    $('.dashboard,.user,.budget,.package').removeClass('active');
                });
            });
        </script>