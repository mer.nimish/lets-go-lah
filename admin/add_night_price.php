<?php
if(!isset($_COOKIE['login'])){
    header('location: index.php');
}
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;

$budget_list = getbudgets();
$pkg_list = getpackages();

if (isset($_POST['createnightpricebtn'])) {
    $pkg_id = $_POST['package'];
    $budget_id = $_POST['budget'];
    $night = $_POST['night'];
    $price = $_POST['price'];

    $last_inserted_id = '';
    $last_inserted_id = addNightPrice($pkg_id, $budget_id, $night, $price);
      
    if ($last_inserted_id > 0 && $last_inserted_id != '') {
        $message = '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Your Form data added successfully.</p> 
						<div class="clearfix"></div>
					</div>';
    } else {
        $message = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.</p>
						<div class="clearfix"></div>
					</div>';
    }
}

include('header.php');
include('left_sidebar.php');
?>

<div class="page-wrapper">
    <div class="container-fluidd">	
        <div class="col-md-12">
            <?php if (isset($successMsg)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left"><?= $successMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorEmailMsg)) { ?>
                <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-alert-circle-o pr-15 pull-left"></i><p class="pull-left"><?= $errorEmailMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorOtherMsg)) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left"><?= $errorOtherMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
        </div>

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Add New Price</h5>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Price</a></li>
                    <li class="active"><span>Add Price</span></li>
                </ol>
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Price Form</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo $message; ?>
                                    <div class="form-wrap">
                                        <form data-toggle="validator" name="createnightform" id="createnightform" method="post" action="#" role="form" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="night_package" class="control-label mb-10">Package Title</label>
                                            <?php 
                                                if($pkg_list){
                                                $res = mysqli_query($conn,$pkg_list);?>
                                                <select class="form-control" name="night_package" id="night_package" required>
                                                    <option value="">Select Package </option>
                                                    <?php while($row = mysqli_fetch_assoc($res)){?>
                                                        <option value="<?= $row['id'];?>"><?= $row['pkg_title'];?></option>
                                                    <?php } ?>
                                                </select>
                                            <?php } ?>
                                                <span class="error" id="pkg_title_error" style="color: red !important;"> </span>
                                            </div>

                                            <div class="form-group">
                                                <label for="night_budget" class="control-label mb-10">Budget Title</label>
                                            <?php 
                                                if($budget_list){
                                                $res = mysqli_query($conn,$budget_list);?>
                                                <select class="form-control" name="night_budget" id="night_budget" required>
                                                    <option value="">Select Budget </option>
                                                    <?php 
                                                    while($row = mysqli_fetch_assoc($res)){?>
                                                        <option value="<?= $row['id'];?>"><?= $row['budget_title'];?></option>

                                                    <?php }?>
                                                </select>
                                                <?php }?>
                                                <span class="error" id="budget_title_error" style="color: red !important;"> </span>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="no_of_nights" class="control-label mb-10">No. of Night</label>
                                                        <select class="form-control" name="no_of_nights" id="no_of_nights" onchange="ajaxcheck()">
                                                            <option value="">Select Night </option>
                                                            <option value="1">1 Night </option>
                                                            <option value="2">2 Night </option>
                                                            <option value="3">3 Night </option>
                                                            <option value="4">4 Night </option>
                                                            <option value="5">5 Night </option>
                                                            <option value="6">6 Night </option>
                                                            <option value="7">7 Night </option>
                                                            <option value="8">8 Night </option>
                                                            <option value="9">9 Night </option>
                                                            <option value="10">10 Night </option>
                                                            <option value="11">11 Night </option>
                                                            <option value="12">12 Night </option>
                                                            <option value="13">13 Night </option>
                                                            <option value="14">14 Night </option>
                                                            <option value="15">15 Night </option>
                                                            <option value="16">16 Night </option>
                                                            <option value="17">17 Night </option>
                                                            <option value="18">18 Night </option>
                                                        </select>
                                                        <span class="error" id="night_error" style="color: red !important;"> </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="price" class="control-label mb-10">Price</label>
                                                        <input type="number" class="form-control" name="price" id="price" required>
                                                        <span class="error" id="price_error" style="color: red !important;"> </span>
                                                    </div>    
                                                </div>
                                            </div>    
                                            <span id="form_error" style="color:red"></span>
                                            <div class="form-group mb-0">
                                                <input type="submit" value="Submit" name="createnightpricebtn" id="createnightpricebtn" class="btn btn-success btn-anim" onclick="check_form()">
                                            </div>                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<style type="text/css">
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0; 
    }
</style>

<?php include('footer.php'); ?>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

<script>
    $(document).ready(function() {
        $("input[type=number]").on("focus", function() {
            $(this).on("keydown", function(event) {
                if (event.keyCode === 38 || event.keyCode === 40) {
                    event.preventDefault();
                }
            });
        });

        $('.budget').addClass('active');
        $('#budget').addClass('collapse in');
        $('.Abudget').css('color', '#000');
        $('.Abudget').css('background-color', '#0fc5bb');
        $('.Vbudget').css('color', '#878787');
        
        $('.budget').addClass('collapsed');

        $('.budget').mouseover(function(){
            $('.dashboard,.user,.package,.order,.gallery').removeClass('active');
        });
        $('.dashboard').mouseover(function(){
            $('.dashboard').addClass('active');
            $('.user,.package,.order,.gallery').removeClass('active');
        });
        $('.user').mouseover(function(){
            $('.user').addClass('active');
            $('.dashboard,.package,.order,.gallery').removeClass('active');
        }); 
        $('.package').mouseover(function(){
            $('.package').addClass('active');
            $('.dashboard,.user,.order,.gallery').removeClass('active');
        });
        $('.order').mouseover(function(){
            $('.order').addClass('active');
            $('.dashboard,.user,.package,.gallery').removeClass('active');
        });
        $('.gallery').mouseover(function(){
            $('.gallery').addClass('active');
            $('.dashboard,.user,.package,.order').removeClass('active');
        });

    });

    /* Exist data check  */
        function ajaxcheck(){
            var night_package = $("#night_package").val();
            var night_budget = $("#night_budget").val();
            var no_of_nights = $("#no_of_nights").val();
            var data = {
                "action" : "check_data",
                "pkg_id" : night_package,
                "budget_id": night_budget,
                "no_of_nights" : no_of_nights
            }
            $.ajax({
                type: "POST",
                dataType:"json",
                url: "ajax/check_night_price.php",
                data: data,
                success: function(data){
                    if (data == false) {
                        alert("Record already exist. Please select another number of nights.");    
                        $('#night_error').html("Please Select Another Night."); 
                        $("#no_of_nights").val("");
                    }else{
                        $('#night_error').html(""); 
                    }
                }
            });
        }
        
    /* Form Validation */
        function check_form(){
            $night_package = $("#night_package").val();
            $night_budget = $("#night_budget").val();
            $no_of_nights = $("#no_of_nights").val();
            $price = $("#price").val();
            var flag = 0;
            
            if ($night_package == '') {
                $('#pkg_title_error').html("Please Select Package.");   
                flag = 1;
            }
            if ($night_budget == '') {
                $('#budget_title_error').html("Please Select Budget.");   
                flag = 1;   
            }
            if ($no_of_nights == '') {
                $('#night_error').html("Please Select Night.");   
                flag = 1;    
            }
            if ($price == '') {
                $('#price_error').html("Please Enter Price.");   
                flag = 1;
            }

            if (flag == 1) {
                $('#form_error').html("Form data is not Accurate.");
                return false;
            }else{
                return true;
            }
        }
</script>