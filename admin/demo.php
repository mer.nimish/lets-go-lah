<?php 
session_start();
include('header.php');
include('left_sidebar.php'); 
//$allcountry = getAllCountry();?>

       <div class="page-wrapper">
         <div class="container-fluid pt-25"> 
            <!-- Custom Filter -->
             <div id="searchEngine" class="collapse collapse-search m-t-10 in">
            <form class="search_property" id="search_property" method="post" action="">
                            <div class="row">
                              <div class="col col-xs-12 col-sm-6 col-lg-4">
                                <div class="form-group mb-0">
                                  <label class="control-label mb-10">State of publication </label>
                                    <select multiple title="Choose" class="selectpicker" name="statepublication[]" id="statepublication">
                                    <optgroup label="All Property">
                                      <option value="1" data-icon="fa fa-fw fa-file" selected="selected">Draft</option>
                                      <option value="1" class="offmarket" data-icon="fa fa-fw fa-low-vision">Off Market</option>
                                      <option value="1" class="public" data-icon="fa fa-fw fa-globe">Public</option>
                                      <option value="1" class="archive" data-icon="fa fa-fw fa-archive">Archive</option>
                                    </optgroup>
                                  </select>
                                </div>
                              </div>
                              <div class="col col-xs-12 col-sm-6 col-lg-4">
                                <div class="form-group">
                                  <label class="control-label mb-10">Transaction Type</label>
                                  <select class="selectpicker" data-style="form-control btn-default btn-outline" name="transectiontype" id="transectiontype">
                                    <optgroup label="">
                                      <option value="">All</option>
                                    </optgroup>
                                    <optgroup label="Sale">
                                      <option value="1"   data-type_transaction="Sale">Sale</option>
                                      <option value="3"  data-type_transaction="Sale">New program</option>
                                      <option value="4"  data-type_transaction="Sale">Sale professional</option>
                                      <option value="2"  data-type_transaction="Sale">Life Time</option>
                                    </optgroup>
                                    <optgroup label="Location">
                                      <option value="5"  data-type_transaction="Location">Location</option>
                                      <option value="6"  data-type_transaction="Location">Location furnished</option>
                                      <option value="7"  data-type_transaction="Location">Location seasonal</option>
                                      <option value="8"  data-type_transaction="Location">Location professional</option>
                                    </optgroup>
                                  </select>
                                </div>
                              </div>  
                              <div class="col col-xs-12 col-sm-6 col-lg-4">
                                <div class="form-group mb-0">
                                  <label class="control-label mb-10">Property type</label>
                                  <select multiple name="propertytype" id="propertytype" data-placeholder="-" class="form-control ">
                                    <optgroup label="">
                                      <option value="">All</option>
                                    </optgroup>
                                    <optgroup label="Appartement">
                                      <option value="1" >Appartement</option>
                                      <option value="2" >Loft</option>
                                      <option value="3" >Penthouse/Roof</option>
                                      <option value="4" >Duplex</option>
                                      <option value="5" >Ground floor</option>
                                      <option value="6" >Room of service</option>
                                   </optgroup>
                                   <optgroup label="House">
                                      <option value="7" >House</option>
                                      <option value="15" >Villa</option>
                                      <option value="8" >Bastide</option>
                                      <option value="9" >castle</option>
                                      <option value="10" >Closed</option>
                                      <option value="12" >Mas</option>
                                      <option value="14" >Ground villa</option>
                                      <option value="13" >Property</option>
                                      <option value="16" >Chalet</option>
                                   </optgroup>
                                   <optgroup label="Building">
                                      <option value="17" >Building plot</option>
                                      <option value="18" >Non constructible land</option>
                                   </optgroup>
                                   <optgroup label="Parking / Garage / Box">
                                      <option value="22" >Parking</option>
                                      <option value="23" >Garage</option>
                                      <option value="24" >Box</option>
                                   </optgroup>
                                   <optgroup label="Office">
                                      <option value="19" >Office</option>
                                   </optgroup>
                                   <optgroup label="Cellar">
                                      <option value="20" >Cellar</option>
                                   </optgroup>
                                   <optgroup label="Local">
                                      <option value="21" >Local</option>
                                   </optgroup>
                                   <optgroup label="Commercial property">
                                      <option value="25" >Commercial property</option>
                                   </optgroup>
                                   <optgroup label="Commercial premises walls">
                                      <option value="27" >Commercial premises walls</option>
                                   </optgroup>
                                   <optgroup label="Assignments of right to the lease">
                                      <option value="26">Assignments of right to the lease</option>
                                   </optgroup>
                                   <optgroup label="Other property">
                                      <option value="28" >Other property</option>
                                      <option value="29" >Building</option>
                                   </optgroup>
                                  </select>
                                </div>
                               </div> 

                            </div>
                            <div class="row">
                              <div class="col col-xs-12 col-sm-6 col-lg-4">
                                <div class="form-group mb-0">
                                  <label class="control-label mb-10">Reference</label>
                                  <input type="text" id="reference" name="reference" class="form-control">
                                </div>
                               </div>
                               <div class="col col-xs-12 col-sm-6 col-lg-4">
                                <div class="form-group">
                                  <label for="" class="control-label mb-10">Country</label>
                                    <select class="form-control selectpicker" id="country" name="country" data-live-search="true">
                                      <option value="">All</option>
                                      <?php 
                                      if(isset($allcountry) && $allcountry != 0){
                                        while ($country = mysqli_fetch_assoc($allcountry)) { ?>
                                          <option value="<?=$country['country_id']?>"><?=$country['country_nicename']?></option>
                                      <?php
                                        }
                                       }?> 
                                    </select>
                                  
                                </div>
                              </div>
                              <div class="col col-xs-12 col-sm-6 col-lg-4">
                                <div class="form-group mb-0">
                                  <label class="control-label mb-10">No. Rooms </label>
                                    <select multiple title="All(8)" name="nubrooms" id="nubrooms" class="selectpicker">
                                      <option value="1" >Studio</option>
                                      <option value="2" >2 rooms</option>
                                      <option value="3" >3 rooms</option>
                                      <option value="4" >4 rooms</option>
                                      <option value="5" >5 rooms</option>
                                      <option value="10" >+5 rooms</option>
                                    
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-lg-4">
                                <div class="form-group mb-0">
                                  <label class="control-label mb-10 text-left">On-line</label>
                                  <input class="form-control input-daterange-datepicker" name="onlinedate" id="onlinedate" type="text" name="daterange" value="" />
                                </div>
                              </div>
                               <div class="col col-xs-12 col-sm-6 col-lg-4">
                                <div class="form-group">
                                  <div class="col-sm-6">
                                    <label class="control-label mb-10 text-left">Min Area</label>
                                    <input type="text" name="minarea" class="form-control" id="minarea" placeholder="min. (m²)">
                                  </div>
                                  <div class="col-sm-6">
                                    <label class="control-label mb-10 text-left">Max Area</label>
                                    <input type="text" name="maxarea" class="form-control" id="maxarea" placeholder="max. (m²)">
                                  </div>
                                </div>
                              </div>
                              <div class="col col-xs-12 col-sm-6 col-lg-4">
                                <div class="form-group mb-0">
                                  <label class="control-label mb-10">Other criteria</label>
                                    <select multiple title="-" name="othercriteria" id="othercriteria" class="selectpicker">
                                      <option value="mixeduse" >Mixed Use</option>
                                      <option value="terrace" >With terrace</option>
                                      <option value="parking" >With Parking</option>
                                      <option value="furniture" >Furniture</option>
                                      <option value="rentalpossible" >Student rental possible</option>
                                      <option value="interesting" >Interesting view</option>                             
                                      <optgroup label="UNDER LAWS:">
                                        <option value="underlaws" >Under laws</option>
                                        <option value="notunderlaws" >Not under laws</option>   
                                      </optgroup>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </form>
                </div>
            <!-- Table -->
            <table id='empTable' class='display dataTable'>
                <thead>
                <tr> 
                    <th>UPD</th>
                    <th>REF</th>
                    <th>DESIGNATION</th>
                    <th>TRANSECTION TYPE</th>
                    <th>LOCATION</th>
                    <th>PRICE/RENT</th>
                    <th>DIFFUSION</th>
                    <th>ROOMS</th>
                    <th>PRICE/M<sup>2</sup></th>
                    <th>Action</th>
                   
                </tr>
                </thead>
                
            </table>
        </div>
        
        <!-- Script -->
        <script>
        $(document).ready(function(){
            var dataTable = $('#empTable').DataTable({
                'processing': true,
                'serverSide': true,
                'serverMethod': 'post',
                'searching': false, // Remove default Search Control
                'ajax': {
                    'url':'ajax/ajaxfile.php',
                    'data': function(data){
                        // Read values
                        
                        var transectiontype = $('#transectiontype').val();
                        var propertytype = [];
                        var propertytype = $('#propertytype').val();
                         var statepublication = [];
                        statepublication= $('#statepublication').val();
                        var reference = $('#reference').val();
                        var country = $('#country').val();
                        var nubrooms = [];
                        var nubrooms = $('#nubrooms').val();
                        var startdate = $('input[name=daterangepicker_start]').val();
                        var enddate = $('input[name=daterangepicker_end]').val();
                        var minarea = $('#minarea').val();
                        var maxarea = $('#maxarea').val();
                        var othercriteria = [];
                        var othercriteria = $('#othercriteria').val();
                       
                        // Append to data
                        data.transectionType = transectiontype;
                        data.proType = propertytype;
                        data.statePublication = statepublication;
                        data.reference = reference;
                        data.country = country;
                        data.nubRooms = nubrooms;
                        data.startDate = startdate;
                        data.endDate = enddate;
                        data.minArea = minarea;
                        data.maxArea = maxarea;
                        data.otherCriteria = othercriteria;
                    }
                },
                    'columns': [
                    { data: 'updated_at' },
                    { data: 'reference' },
                    { data: 'address' },
                    { data: 'transaction_type' },
                    { data: 'building_id' },
                    { data: 'price' },
                    { data: 'diffusion' },
                    { data: 'num_rooms' },
                    { data: 'price/m' },
                    { data: 'action' },

                ]
                
            });

            $('#searchEngine input[type=text]').keyup(function(){
                dataTable.draw();
            });

            $('#searchEngine select').change(function(){
                dataTable.draw();
            });
            $(document).on('click', '.applyBtn', function(event) {
               dataTable.draw(); 
            }); 
        });
        </script>
 <?php include('footer.php');?>