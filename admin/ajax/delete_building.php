<?php

require '../config/config.php';
require '../model/model.php';
global $conn;
$response = array();

if (isset($_POST['action']) && $_POST['action'] == 'delete_building' && isset($_POST['building_id']) && $_POST['building_id'] != '') {
    $delete_res = removeBuildingData($_POST['building_id']);
    if ($delete_res == '1') {

        $message = '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Yay! Building has been deleted successfully.</p> 
						<div class="clearfix"></div>
					</div>';
        $success = "true";
    } else {
        $message = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.</p>
						<div class="clearfix"></div>
					</div>';
        $success = "false";
    }
    $response = array('message' => $message, 'success' => $success);
    echo json_encode($response);
}
?>
