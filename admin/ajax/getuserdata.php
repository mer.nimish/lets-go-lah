<?php

require '../config/config.php';
require '../model/model.php';
global $conn;

$data = $_POST['user_id']; 
$data_arr = explode(',', $data);
$user_id  = current($data_arr);
$username = end($data_arr);

if (isset($user_id) && $user_id != '') {
    $user_res = getuserById($user_id);
    echo json_encode($user_res);
}
?>
