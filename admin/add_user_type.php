<?php
if(!isset($_COOKIE['login'])){
    header('location: index.php');
}
session_start();
require 'config/config.php';
require 'model/model.php';
global $conn;

if (isset($_POST['createusertypebtn']) && $_POST['createusertypebtn'] == 'Submit'){ 

    $user_type = $_POST['user_type'];
    if (isset($_POST['add_contacts']) == "on") {
        $add_contacts = "1";
    } else {
        $add_contacts = "0";
    }
    if (isset($_POST['add_properties']) == "on") {
        $add_properties = "1";
    } else {
        $add_properties = "0";
    }
    if (isset($_POST['view_properties']) == "on") {
        $view_properties = "1";
    } else {
        $view_properties = "0";
    }

    $last_inserted_id = '';
    $last_inserted_id = addUserType($user_type, $add_contacts, $add_properties, $view_properties);

    if ($last_inserted_id > 0 && $last_inserted_id != '') {
        $message = '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Yay! User type has been added successfully.</p> 
						<div class="clearfix"></div>
					</div>';
    } else {
        $message = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.</p>
						<div class="clearfix"></div>
					</div>';
    }
}

if (isset($_POST['btnedit']) && $_POST['btnedit'] == 'Save Changes') 
{
    $user_type = $_POST['user_type'];
    if (isset($_POST['add_contacts']) == "on") {
        $add_contacts = "1";
    } else {
        $add_contacts = "0";
    }
    if (isset($_POST['add_properties']) == "on") {
        $add_properties = "1";
    } else {
        $add_properties = "0";
    }
    if (isset($_POST['view_properties']) == "on") {
        $view_properties = "1";
    } else {
        $view_properties = "0";
    }
    $utid = $_POST['utid'];
    $last_inserted_id = '';
    $last_inserted_id = editUserType($utid, $user_type, $add_contacts, $add_properties, $view_properties);

    if ($last_inserted_id > 0 && $last_inserted_id != '') {
        $message = '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Yay! User type has been updated successfully.</p> 
						<div class="clearfix"></div>
					</div>';
    } else {
        $message = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.</p>
						<div class="clearfix"></div>
					</div>';
    }
}
$allusertypeslist = getusertypes();
include('header.php');
include('left_sidebar.php');
?>

<div class="page-wrapper">
    <div class="container-fluid pt-25">	
        <div class="col-md-12">
            <?php if (isset($successMsg)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left"><?= $successMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorEmailMsg)) { ?>
                <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-alert-circle-o pr-15 pull-left"></i><p class="pull-left"><?= $errorEmailMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
            <?php if (isset($errorOtherMsg)) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left"><?= $errorOtherMsg ?></p>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
        </div>

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Create New User Type</h5>
            </div>
        </div>
        <!-- /Title -->

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">User Type Form</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-8">
                                    <?php echo $message; ?>
                                    <div class="form-wrap">
                                        <form data-toggle="validator" name="createusertypeform" id="createusertypeform" method="post" action="#" role="form">
                                            <?php
                                            if (isset($_REQUEST['utid']) && $_REQUEST['utid'] != '') {
                                                $utdata = getUserTypedatabyID($_REQUEST['utid']);

                                                $add_contacts = '';
                                                $add_properties = '';
                                                $view_properties = '';
                                                echo "<input type='hidden' name='utid' value='" . $_REQUEST['utid'] . "' />";
                                                while ($utypeslist = mysqli_fetch_assoc($utdata)) {
                                                    
                                                    if(isset($utypeslist['user_type']) && $utypeslist['user_type'] != ''){
                                                        $usertype = $utypeslist['user_type'];
                                                        if($usertype == 'Super Admin'){
                                                           echo "<script>window.location='view_user_type.php'</script>";exit;
                                                        }
                                                    }
                                                    
                                                    
                                                    if (isset($utypeslist['add_contacts']) && $utypeslist['add_contacts'] != '') {
                                                        if ($utypeslist['add_contacts'] == '1') {
                                                            $add_contacts = 'checked';
                                                        }
                                                    }
                                                    if (isset($utypeslist['add_properties']) && $utypeslist['add_properties'] != '') {
                                                        if ($utypeslist['add_properties'] == '1') {
                                                            $add_properties = 'checked';
                                                        }
                                                    }
                                                    if (isset($utypeslist['view_properties']) && $utypeslist['view_properties'] != '') {
                                                        if ($utypeslist['view_properties'] == '1') {
                                                            $view_properties = 'checked';
                                                        }
                                                    }
                                                }
                                            }
                                            ?>      
                                            <div class="form-group">
                                                <label for="user_type" class="control-label mb-10">User Type</label>
                                                <input type="text" class="form-control" name="user_type" id="user_type" placeholder="User Type" value="<?=$usertype?>" required>
                                            </div>
                                            <div class="form-group">
                                                <div class="checkbox checkbox-primary">
                                                    <input id="add_contacts" type="checkbox" name="add_contacts" <?= $add_contacts ?> >
                                                    <label for="add_contacts"> add contacts </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="checkbox checkbox-primary">
                                                    <input id="add_properties" type="checkbox" name="add_properties" <?= $add_properties ?> >
                                                    <label for="add_properties"> add properties </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="checkbox checkbox-primary">
                                                    <input id="view_properties" type="checkbox" name="view_properties" <?= $view_properties ?> >
                                                    <label for="view_properties"> view properties </label>
                                                </div>
                                            </div>                                            

                                            <div class="form-group mb-0">
                                                <?php
                                                if (isset($_REQUEST['utid']) && $_REQUEST['utid'] != '') {
                                                    ?>                                                        
                                                    <input type="submit" value="Save Changes" name="btnedit" id="btnedit" class="btn btn-success btn-anim">
                                                <?php } else {
                                                    ?>
                                                    <input type="submit" value="Submit" name="createusertypebtn" id="createusertypebtn" class="btn btn-success btn-anim">
                                                <?php } ?>
                                            </div>                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php include('footer.php'); ?>