<?php

session_start();
require '../config/config.php';
require '../config/resize-class.php';
require '../model/model.php';
global $conn;
if (isset($_SESSION['first_name']) && isset($_SESSION['last_name']) && $_SESSION['add_contact'] == '1') {
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
} else {
    echo "<script>window.location='index.php'</script>";
    exit;
}

if (isset($_POST['createfoldername']) && $_POST['createfoldername'] != '') {
    $foldername = $_POST['createfoldername'];
    $msgdata = '';

    if (!is_dir(MEDIA_IMAGE_PATH . "/" . $foldername)) {
        //Directory does not exist, so lets create it.
        mkdir(MEDIA_IMAGE_PATH . "/" . $foldername, 0755, true);
        chmod(MEDIA_IMAGE_PATH . "/" . $foldername, 0777);
    }

    $last_inserted_id = '';    
    $last_inserted_id = addFolderName($foldername);

    if ($last_inserted_id > 0 && $last_inserted_id != '') {
        $message = '<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">Success! Contact has been added successfully.</p> 
						<div class="clearfix"></div>
					</div>';
    } else {
        $message = '<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="zmdi zmdi-block pr-15 pull-left"></i><p class="pull-left">Opps! Somthing went wrong.Please try again.</p>
						<div class="clearfix"></div>
					</div>';
    }
    //}
}