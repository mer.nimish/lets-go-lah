<?php

require '../config/config.php';
require '../model/model.php';
global $conn;
$response = array();
if (isset($_POST['action']) && $_POST['action'] == 'update_status' && isset($_POST['user_id']) && $_POST['user_id'] != '') {
	$update_res = updateUserStatus($_POST['user_id'],$_POST['user_status']);
    if ($update_res) {
        echo json_encode($update_res);
    } 
}
?>
