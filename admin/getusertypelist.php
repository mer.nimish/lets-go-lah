<?php
if(!isset($_COOKIE['login'])){
    header('location: index.php');
}
session_start();
require 'config/config.php';
require 'config/constant.php';
require 'config/resize-class.php';
require 'model/model.php';
global $conn;

if (isset($_SESSION['first_name']) && isset($_SESSION['last_name'])) {
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
} else {
    echo "<script>window.location='index.php'</script>";
    exit;
}

if (isset($_POST["contype"])) {
    //$rowdata = array();
    $contype = $_POST["contype"];
    $query = "SELECT * FROM tbl_user_type WHERE user_type LIKE ".  "'$contype'";    
    $res = mysqli_query($conn, $query);
    $rowcount = mysqli_num_rows($res);

    if ($rowcount > 0) {
        $rowC = mysqli_fetch_assoc($res);
        $query2 = "SELECT user_id,first_name,last_name FROM tbl_user WHERE user_type_id = '" . $rowC['user_type_id'] . "'";        
        $result = mysqli_query($conn, $query2);
        $i = 0;
        while ($row = mysqli_fetch_array($result)) 
        {
            $temp[$i]['first_name'] = $row['first_name'];
            $temp[$i]['last_name'] = $row['last_name'];                        
            $temp[$i]['user_id'] = $row['user_id'];                        
            $i++;
        }
        foreach ($temp as $value) {
             $fname = $value['first_name'];
             $lname = $value['last_name'];
             $uid = $value['user_id'];
             $username = $fname.' '.$lname;            
             $rowdata .= "<option value="."'$uid'".">".$username."</option>";
        }              
    }else{
       $rowdata = array(); 
    }    
    echo json_encode($rowdata);
    exit;
}
